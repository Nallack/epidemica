# ArboVirus C++ Simulator - Working Guide

This is the user manual of ordinary usage
for Arbo Virus Simulator. The following sections
will cover prepare, build and run this simulator.

For full document, please go to [the actual document space](Docs/tableOfContents_github.md)

## Basic Features
* High performance optimisation using OpenMP
* cross-platform supported by CMake

## Dev Environment Setup
The following packages are required for compile, or running this project. install the following libraries with your package manager, or follow instructions from your system vendor:

* CMake, version 3.2 or later
* Build-Essential packages
* OpenMP (Included in general compilers)
* MPI, suggest OpenMPI
* Boost, 1.57.0 or later (Optional)

To install all these package, please follow instructions from you system vendor.
It recommended that you use msys2 on Windows, brew on OSX, pacman or apt-get on GNU based linux system and g++ compiler in order to utilize the package manager.

For GNU based system, using pacman or apt-get can simply having all dependences resolve by running these commands:

- apt-get update
- apt-get install cmake build-essential libboost-dev-all

For Red Hat Enterprise Linux, you need to complete the following step to make compile work.

- Apply for Red Hat Developer Subscription (Free of charge)
- login your account with developer subscription onto your subscription manager.
- Enable Developer Toolset (DTS) for your system
- update and install components from DTS.
- more information, please checkout https://access.redhat.com/documentation/en-US/Red_Hat_Developer_Toolset/6/html/6.1_Release_Notes/index.html

For other Linux or other system, please check your system vendor for further information.

Note: as part of turning this simulator into a single cmake project, support for visual studio has been added
To setup your environment, please remember to add BOOST_ROOT, GTEST_ROOT, etc. to your environment variables and restart the IDE.


## Load essential third-party modules
This virus simulator relay on multiple third-party modules, including easylogging++, GTest framework, MatPlot
Library for C++ and rapidjson. These modules, by default, not loaded in this project folder.

You need to perform manual actions to include those modules. In particular, you need:

- going to the root of this project
- run `git submodule update --init --recursive` using `git`.

Please note that you need `git` installed in your system before you perform step above.

## Running CMake and Compile

### About CMake
This project has been setup with CMake, so it should support cross-platform builds, and be should be fixed otherwise.

CMake is a tool to generate project files for other build tools to run, it does not actually compile the code.

This project has been tested on the following operating system:

- GNU Linux System (Debian, Ubuntu)
- RPM based system (RedHat Enterprise Linux, CentOS)
- Windows MSVC
- Windows MinGW
- OSX

### Compile the simulator in regular environment
To compile it, first make sure libraries needed has been installed.

After all libraries installed, go to the root of this project, and create a folder. In this example, we create a folder called `build`.

Go to `build/`, and run the command: `cmake ../ -G <Generator Name>`. While generator name can be defined, or this can be leave blank.

For advanced user, you can make a folder in somewhere you want and perform cmake to related or absolute path to this project.

After `cmake` finish, run `make` to perform compiling.

Guides below should help when this is not so straightforward.

If you use the "Unix Makefiles" generator, simply run run `make` in the build directory.

If you use an IDE generator like "Visual Studio 15 2017 x64",
open the solution file in the build directory,
as when you open the project it automatically help you build.

### Compile in linux environment using bash script
A script in root directory of this project called build-regular.sh, can help you build your progress by performing:

* check if cmake, boost, numpy, Python is installed
* create a folder called "builds" if not created
* get into folder "builds"
* perform cmake, abort when error occur
* perform make after cmake success

### Compile on Magnus
Compiling over Magnus is different than compile it over local machine. This apply to every supercomputers having multiple compile environment, module management and SLURM workload management. This guide only shows instruction compiling on Magnus, for other supercomputers please consult with supercomputing centre representatives.

To enable compile it, make sure you have the following module loaded:

- CMake/3.4.1
- Boost/1.57.0
- GNU Program Environment (PrgEnv-gnu)
- gcc/4.9.2

This can be achieve by module load / swap, by running the following command:

- `module swap PrgEnv-cray PrgEnv-gnu`
- `module swap gcc gcc/4.9.2`
- `module load cmake/3.4.1`
- `module load boost/1.57.0`  

When finished executing these commands from your logon terminal, you are ready to run `cmake`. after `cmake` finished, you can run `make` to compile the code.

### Compile on Magnus using bash script
A script in root directory of this project called build-magnus.sh, can help you build your progress by performing:

* swap and load all required modules in Magnus
* create a folder called "builds" if not created
* get into folder "builds"
* perform cmake, abort when error occur
* perform make after cmake success

## Usage
When finished compiling, a folder `bin` will appear in your building directory. This directory contains the executable `VirusSimulator` and a few set of sample data. In the following examples, you are supposed to executed inside `bin/` of your compile directory.

### Run with default sample data and default setting

  - `./VirusSimulator`

### Run with different scenario
You can define your scenario file if you want to use your own set of data. You should use absolute path or related path regarding to your working directory if you want to choose your scenario. Example file: in 523/test10/test1.param of "bin"

  - `./VirusSimulator --scenario=523/test10/test1.param`

### Run with experimental OpenMP optimisation
Please note, to make sure you have cores you want to use for running, you need to define number of OpenMP threads by defining `OMP_NUM_THREADS`. In this example, we use 24 threads for 24 cores in Magnus.

  - `export OMP_NUM_THREADS=24`
  - `./VirusSimulator --implementation=OMP`

### run with experimental epidemica curve output for windowed environment
This is available only on windowed environment, like Gnome desktop environment in Linux.

  - `./VirusSimulator --epicurve`

### Additional requirement to run over Pawsey Magnys
Running on Pawsey Magnus requires additional steps. As you need to allocate resources and run faster, you need to perform the following steps:

#### move everything into scratch
Since Pawsey has a faster file input/output in scratch other than your regular personal directory, moving to scratch is faster. In this example, we assume you are having a folder called "epidemica" for this project.

  - `mv epidemica/ $MYSCRATCH`

Once you perform this command, all your project file are already stored on scratch.
WARNING: scratch file system has an expire time for your file. Your file will be removed if inactive for 30 days.

#### compose a bash script for launching onto Pawsey
you need to let the supercomputer itself to run a project for you, that is, you need to write a script and launch it to pawsey, let them run it for you.
To achieve this, suggested to write a script in builds/bin/, with a file name you desired, with the following:

```bash
#!/bin/bash --login

#SBATCH --nodes=1
#SBATCH --account=pawsey0150
#SBATCH --job-name=virusSimulator
#SBATCH --time=00:15:00
#SBATCH --partition=debugq

module swap PrgEnv-cray PrgEnv-gnu
module swap gcc gcc/4.9.2
module load cmake/3.4.1
module load boost/1.57.0

export OMP_NUM_THREADS=24
aprun -n 1 -N 1 -d $OMP_NUM_THREADS ./VirusSimulator --implementation=OMP
```

where in this script, you need to modify the following variables before deployment:

* nodes=1: how many nodes you want to allocate, since only OpenMP is enabled and therefore using more than 1 node is worthless and useless.
* account=pawsey0150: defines your research group account, only research group associated with your account can be use.
* job-name=virusSimulator: the name you want to show up when any other people load queue information from supercomputer.
* time=00:15:00: only 15 minutes are used in here, if a larger scale scenario is in use, suggested to use longer time. Please note: workload manager will try fit your queue into supercomputer by checking how much resources and time you are using. the less time you use, sooner you will fit.
* partition=debugq: you can use debugq, where only up to 1 hour available for application debugging, or use workq, the working queue, with maximum 1 day of runtime. Generally, running in debugq is sooner than run in workq since debugq itself is higher demand.
* OMP_NUM_THREADS=24: number of OpenMP threads you want to use. 24 is maximum per node.
* at the end of aprun command, you can use any regular argument from list above.

#### post your script to supercomputer.
When you finished up writing the script, run sbatch with your script and your script will be execute automatically when your queue is fit in the supercomputer. In this example, we assume you have a script name `vsrun`:
  - `sbatch vsrun`

## Installing Libraries for Windows via MinGW
How to setup MinGW and libs on windows

follow this trusty site: http://www.math.ucla.edu/~wotaoyin/windows_coding.html
### Steps
- install msys2. 
  - honest to god, this is the only version of ming32 and ming64 you want installed
  - the number of libraries available via pacman is much better than the mingw-get alternative
  - projects written for linux will compile in the msys2 bash, and into windows binaries

- add msys64/mingw64/bin to your PATH
  - put it towards the end, you will want all your normal processes to find dlls compiled in msvc first
  - msys2 bash on the contrary will prioritize binaries in its install location
  - this method lets you do mingw builds in msys2 bash, and msvc builds in cmd shells. No more cygwin :)

## Boost (Required)
### MinGW Windows Install

install boost from msys2

- run `pacman -Ss boost` boost
- run `pacman -S <name>`
- run `./tools/build/bootstrap msvc`
- run `./tools/build/b2 toolset=msvc warnings=off threading=multi link=static variant=release stage`
- Add BOOST_ROOT to your environment variables pointing to the location of your boost folder

### MSVC Windows Install
I recommend building everything in x64, but Win32 is possible, check link below

- Download the latest boost source to some location
- Run Visual Studio command prompt
  - for VS2017, run `"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" x64`
- run `bootstrap.bat msvc`
- run `tools\build\b2 toolset=msvc warnings=off threading=multi link=static variant=release stage`
- Add BOOST_ROOT to your environment variables pointing to the location of your boost folder

maybe try using these settings: [link](https://studiofreya.com/2015/05/20/building-boost-1-58-for-32-bit-and-64-bit-architectures-with-visual-studio-2013/)


## Boost MPI (Optional)
### MinGW Windows Install
Make sure you installed MPI first as described in this readme

download the latest boost source to compile boost_mpi

- run: `./tools/build/bootstrap gcc`
- run: `./tools/build/b2 toolset=gcc warnings=off threading=multi link=static variant=release --prefix=./build/ mpi`
- copy the libboost_mpi-mt.a and .dll.a to ming64/lib/, .dll to ming64/bin/


## MPI (optional)
### Windows MinGW Install
[install the latest msmpi sdk and runtime (link to v8)](https://www.microsoft.com/en-us/download/details.aspx?id=54607)

  - do the dll linkage thing
    - run: gendef msmpi.dll
    - run: dlltool -d msmpi.def -D msmpi.dll -l libmpi.a
    - add libmpi.a file to mingw64/lib/
      - no need to copy msmpi.dll to bin as shell and msys2 check windows directories now after adding to PATH
  - do the header tweak from the website [link](http://www.math.ucla.edu/~wotaoyin/windows_coding.html)
  - add mpi.h to mingw64/include

## GoogleTest (Optional)
### Windows MSVC Install

- Clone the googletest repository
- Run it via cmake
- Open the solution in Visual Studio
  - switch to Release build
  - run the Install project
    - Should install to Program Files somewhere
  - Add environment variable GTEST_ROOT with the install path

### Windows MinGW Install

- should be available via pacman

## Python Matplot Library (Optional)
This simulation enables you to use to quote matplot library, showing SEIR result after simulation finish,
which you can use it when you are having a small scale simulation.

(Warning: Please do not enable this module when you are deploying this simulation into a super computer,
as that would suspend resources allocated to you into idle when plot is showing up, wasting your allocation
and blocking other researches.)

To make it working, you need to have:

- Python 2.7 or later in Python 2, or
- Any version of Python 3,
- MatPlotLib installed in your Python.

For installing Python, please follow guides from Python websites. Or if you are using a super computer,
follow guides from your super computer operator to see how to enable Python.

Installation of `matplotlib` requires you to have `pip`, the python package installer replaced `easy-install`.
Starting from Python 3.4 or Python 2.7.9, `pip` has been ship with official python binary.
For any version earlier than that, please consider upgrading python, or follow instruction installing `pip` on
the internet. Following instructions assumes you have pip installed.

### Install Python MatPlot Library on Windows
When installing Python on Windows, make sure you have privileges installing packages on you Windows, then follow:

- Start your command prompt as a system administrator
- run the following command: `python -m pip install matplotlib`

### Install Python MatPlot Library on any Unix system:
Same as installing in Windows, make sure you are in the user group that can use `sudo`, or you are root user.

- for root user, run the following command: `pip install matplotlib`
- for user having sudo privileges, run the following command: `sudo pip install matplotlib`. It may ask you your password for logged user in the system.


## Build note 1 (irrelevent/ignore)
as of boost 1_63_0: boost has deprecated function calls to auto_ptr, and can't compile boost_mpi correctly

this is becoming really hard to make work on mingw, but on normal distros it should be cake

A fix suggestion is:

- downloading the same version source code
- modify the build configuration to point to msmpi, or find mpich2 for windows
- build the mpi module
- copy it back into the msys2 x64 lib directory

## Build note 2
to fix current boost mpi build issues on mingw

- in boost_1_63_0\libs\serialization\build\Jamfile.v2, line 109, add: <define>BOOST_SERIALIZATION_SOURCE

Let me know if you want my compiled versions of boost binaries

## Building

CMake generates project files for other build systems and IDEs, however, if you use an IDE, you will need to update the cmakelists.txt when
appropriate. (Wildcards are currently being used for filenames, so just make sure you add new source files into the correct location)

Make sure to run cmake whenever you pull code.

# Virus Models

While every different virus module having different process, including but not limit to:

- Regular Human Movement
- Vector Movement
- Host Infection
- Infection in vector
- Population change

It is recommended to change the code yourself, before executing this simulator, to fit
any model of your choice.


 
## Testing Framework
- CTest
  - CTest is only a plugin for cmake, it works with a variety of unit test frameworks :)
- GoogleTest (GTest)
  - Currently being used, it seems alright and has a VS IDE integration


## Other information

### About Test Data
  - the `actual` file include the actual data simulating in Thailand area.
  - the `523` file include data generated randomly with large scenarios
  - the others are some basic test data

## Project Support
The following are participates involving in this simulator, which are all students from the University of Western Australia.

- Simon Xie, student number 21259203
- Callan Gray, student number 21341958
- QIANG SUN, student number 21804416
- Prerna Toppo, student number 21996929
- Jiaojie Wei, student number 21553103

For any questions in this project, please contact anyone in the list above by email. (Email address: student_number@student.uwa.edu.au)