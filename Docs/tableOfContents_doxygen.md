# 0 Table of Contents (For Doxygen) {#toc-doxygen}

The following is the table of contents for Arbo Virus Documents. Click on the link to jump to the page.

This table of contents only works on Doxygen.

if using github markdown or similar, please [use this table of contents](tableOfContents_github.md) instead.

- [1 Introduction](@ref intro)
- [2 Build](@ref build-overall)
  - [2.1 Build on local environment](@ref build-general)
  - [2.2 Build on Magnus, Pawsey Supercomputer Center](@ref build-magnus)
  - [2.3 About CMake](@ref build-aboutCMake)
- [3 Simulator Usage](@ref run-overall)
  - [3.1 Run simulation over Magnus](@ref run-magnus)
- [4 Maintenance Manual ](@ref maintenance-overall)
  - [4.1 System Architecture](@ref architecture)
  - [4.2 Code Convertions](@ref codeConvert)
  - [4.3 Logging Modification and Usage](@ref logging)
  - [4.4 Git Usage](@ref git)
  - [4.5 Optimisations Notes](@ref optimisation)
- [5 Guideline for Extend program](@ref tute-newProcess)
- [6 Testing And Validation](@ref testValidate)