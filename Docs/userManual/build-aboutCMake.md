# 2.3 About CMake {#build-aboutCMake}

This simulator uses the cmake build system. Cmake code is not the same as a Makefile or project file, but describes how to generate a Makefile or project file for any environment.

One of the main challenges of C/C++ project is building and linking all the required libraries for a particular machine, and minimize
the amount of problems that can occur allong the way. All major C/C++ libraries provide cmake find scripts (e.g. `FindOPENMP.cmake`, `FindBOOST.cmake`) or unofficial versions can be found on the web, and these will effectively describe how to link the library correctly.

While intimidating at first, CMake in the long run saves a lot of time dealing with quirks about how to achieve particular build steps and has a very large and active community to provide help writing cmakefiles.

## Running Manually

- Install cmake for command line (cmake-gui is also available)
  - cmake is also available on Pawsey machines
- make a directory for the build files
  - typically make a directory called `build` (e.g. `Epidemica/build/`)

This is known as an "out-of-source-build". Essentially this has a major advantage that should there be a bug
in the cmakefiles, the cmake building will never modify the original source directory and files.

- navigate into the build directory with `cd build`
- use command `cmake [source directory]`
  - (e.g. `cmake ../`)

This command says build the project at `../` and put the generated makefiles in the current working directory.

- Depending on your system, you may also want to specify the generator type
  - (e.g. `cmake ../ -G "Unix Makefiles"`)
  - entering an invalid generator will bring up a list of all available generators

Once generated simply use either run `make` or any other build system of your choosing.