# 2.2 Build on Magnus, Pawsey Supercomputer Center {#build-magnus}
This guide will apply to Magnus supercomputer, but generally it can be run through any Cray supercomputers.

Please contact your supercomputer organisation before you trying to execute this program over supercomputer.

## 2.2.1 Manually Build
Compiling over Magnus is different than compile it over local machine. This apply to every supercomputers having multiple compile environment, module management and SLURM workload management. This guide only shows instruction compiling on Magnus, for other supercomputers please consult with supercomputing centre representatives.

To enable compile it, make sure you have the following module loaded:

- CMake/3.4.1
- Boost/1.57.0
- GNU Program Environment (PrgEnv-gnu)
- gcc/4.9.2

This can be achieve by module load / swap, by running the following command:

- `module swap PrgEnv-cray PrgEnv-gnu`
- `module swap gcc gcc/4.9.2`
- `module load cmake/3.4.1`
- `module load boost/1.57.0`  

When finished executing these commands from your logon terminal, you are ready to run `cmake`. after `cmake` finished, you can run `make` to compile the code.

## 2.2.2 Build with MPI support
Building with MPI support requires additional effort on Magnus as we need to use GNU compiler and Cray wrapper.

Replace the command on 2.2.1, you need to execute the following:
- `module swap PrgEnv-cray/5.2.82 PrgEnv-gnu`
- `module swap gcc gcc/4.9.2`
- `module load boost/1.57.0  cmake/3.4.1 python/2.7.10`
- `export CRAYPE_LINK_TYPE=dynamic`
- `export CC=cc`
- `export CXX=CC`

## 2.2.3 Compile on Magnus using bash script
A script in root directory of this project called build-magnus.sh, can help you build your progress by performing:

* swap and load all required modules in Magnus
* create a folder called "builds" if not created
* get into folder "builds"
* perform cmake, abort when error occur
* perform make after cmake success

Note: as the unstability of Cray Wrapper, the MPI support is not included in the script.

You can add the script manually by editing the script.