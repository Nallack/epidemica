# 2.1 Build on local environment {#build-general}

## 2.1.1 Build on Linux Environment

### Install requirement Package
The following packages are required for compile, or running this project. install the following libraries with your package manager, or follow instructions from your system vendor:

* CMake, version 3.2 or later
* Build-Essential packages
* OpenMP (Included in general compilers)
* MPI, suggest OpenMPI

To install all these package, please follow instructions from you system vendor.
It recommended that you use msys2 on Windows, brew on OSX, pacman or apt-get on GNU based linux system and g++ compiler in order to utilize the package manager.

For GNU based system, using pacman or apt-get can simply having all dependences resolve by running these commands:

- apt-get update
- apt-get install cmake build-essential openmpi-dev

For Red Hat Enterprise Linux, you need to complete the following step to make compile work.

- Apply for Red Hat Developer Subscription (Free of charge)
- login your account with developer subscription onto your subscription manager.
- Enable Developer Toolset (DTS) for your system
- update and install components from DTS.
- more information, please checkout https://access.redhat.com/documentation/en-US/Red_Hat_Developer_Toolset/6/html/6.1_Release_Notes/index.html

For other Linux or other system, please check your system vendor for further information.

Note: as part of turning this simulator into a single cmake project, support for visual studio has been added
To setup your environment, please remember to add BOOST_ROOT, GTEST_ROOT, etc. to your environment variables and restart the IDE.

### load third-party modules

This virus simulator relay on multiple third-party modules, including easylogging++, GTest framework, MatPlot
Library for C++ and rapidjson. These modules, by default, not loaded in this project folder.

You need to perform manual actions to include those modules. In particular, you need:

- going to the root of this project
- run `git submodule update --init --recursive` using `git`.

Please note that you need `git` installed in your system before you perform step above.

### Compile the simulator in regular environment
To compile it, first make sure libraries needed has been installed.

After all libraries installed, go to the root of this project, and create a folder. In this example, we create a folder called `build`.

Go to `build/`, and run the command: `cmake ../ -G <Generator Name>`. While generator name can be defined, or this can be leave blank.

For advanced user, you can make a folder in somewhere you want and perform cmake to related or absolute path to this project.

After `cmake` finish, run `make` to perform compiling.

Guides below should help when this is not so straightforward.

If you use the "Unix Makefiles" generator, simply run run `make` in the build directory.

If you use an IDE generator like "Visual Studio 15 2017 x64",
open the solution file in the build directory,
as when you open the project it automatically help you build.

### Build on Linux using bash script
A script in root directory of this project called build-regular.sh, can help you build your progress by performing:

* check if cmake, boost, numpy, Python is installed
* create a folder called "builds" if not created
* get into folder "builds"
* perform cmake, abort when error occur
* perform make after cmake success

## 2.1.2 Build on Windows System
### Installing Libraries for Windows via MinGW
We are highly recommended to use Visual Studio when you want to start simulating, as it provides easy to use
and fast compiling for this project. The steps are follow below:
- Download Visual Studio Community through Microsoft webpage
- Start the installation
- When ask for IDE, select "C++ development for Linux", together with Git.
- continue installation
- Open Visual Studio, Go to Team Explorer, press "clone" on the section, fill in the git repository information
- The system will help you clone the project and Run CMake.
- Press "Build" to build the project.