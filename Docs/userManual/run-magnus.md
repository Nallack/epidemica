# 3.1 Run simulation over Magnus {#run-magnus}
Running on Pawsey Magnus requires additional steps. As you need to allocate resources and run faster, you need to perform the following steps:

## 3.1.1 move everything into scratch
Since Pawsey has a faster file input/output in scratch other than your regular personal directory, moving to scratch is faster. In this example, we assume you are having a folder called "epidemica" for this project.

  - `mv epidemica/ $MYSCRATCH`

Once you perform this command, all your project file are already stored on scratch.
WARNING: scratch file system has an expire time for your file. Your file will be removed if inactive for 30 days.

## 3.1.2 compose a bash script for launching onto Pawsey
you need to let the supercomputer itself to run a project for you, that is, you need to write a script and launch it to pawsey, let them run it for you.
To achieve this, suggested to write a script in builds/bin/, with a file name you desired, with the following:

```bash
#!/bin/bash --login

#SBATCH --nodes=1
#SBATCH --account=pawsey0150
#SBATCH --job-name=virusSimulator
#SBATCH --time=00:15:00
#SBATCH --partition=debugq

module swap PrgEnv-cray PrgEnv-gnu
module swap gcc gcc/4.9.2
module load cmake/3.4.1
module load boost/1.57.0

export OMP_NUM_THREADS=24
aprun -n 1 -N 1 -d $OMP_NUM_THREADS ./VirusSimulator --implementation=OMP
```

where in this script, you need to modify the following variables before deployment:

* nodes=1: how many nodes you want to allocate, you can choose to use more nodes but it may cause you spend longer time to run the simulation.
* account=pawsey0150: defines your research group account, only research group associated with your account can be use.
* job-name=virusSimulator: the name you want to show up when any other people load queue information from supercomputer.
* time=00:15:00: only 15 minutes are used in here, if a larger scale scenario is in use, suggested to use longer time. Please note: workload manager will try fit your queue into supercomputer by checking how much resources and time you are using. the less time you use, sooner you will fit.
* partition=debugq: you can use debugq, where only up to 1 hour available for application debugging, or use workq, the working queue, with maximum 1 day of runtime. Generally, running in debugq is sooner than run in workq since debugq itself is higher demand.
* OMP_NUM_THREADS=24: number of OpenMP threads you want to use. 24 is maximum per node.
* at the end of aprun command, you can use any regular argument from Overall Simulation Usage.
* -n: how many MPI processes will be totally executed 
* -N: how many MPI process will be executed within one node.
  * Remember, each node in Magnus contains 24 cores.
  * Using 2 nodes will using totally 48 cores.
  * if setting OMP_NUM_THREADS=12, -n 4 -N 2, this will be totally execute 4 MPI processes, while 2 MPI processes for each node, and each MPI process will use 12 cores for OpenMP execution.
  * This will gain you ability of optimising resources you can use.

## 3.1.3 post your script to supercomputer.
When you finished up writing the script, run sbatch with your script and your script will be execute automatically when your queue is fit in the supercomputer. In this example, we assume you have a script name `vsrun`:
  - `sbatch vsrun`