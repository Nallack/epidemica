# 3 Simulator Usage {#run-overall}
When finished compiling, a folder `bin` will appear in your building directory. This directory contains the executable `VirusSimulator` and a few set of sample data. In the following examples, you are supposed to executed inside `bin/` of your compile directory.

## 3.0.1 Run with default sample data and default setting

  - `./VirusSimulator`

## 3.0.2 Run with different scenario
You can define your scenario file if you want to use your own set of data. You should use absolute path or related path regarding to your working directory if you want to choose your scenario. Example file: in 523/test10/test1.param of "bin"

  - `./VirusSimulator --scenario=523/test10/test1.param`

## 3.0.3 Run with experimental OpenMP optimisation
Please note, to make sure you have cores you want to use for running, you need to define number of OpenMP threads by defining `OMP_NUM_THREADS`. In this example, we use 24 threads for 24 cores in Magnus.

  - `export OMP_NUM_THREADS=24`
  - `./VirusSimulator --implementation=OMP`

## 3.0.4 run with experimental epidemica curve output for windowed environment
This is available only on windowed environment, like Gnome desktop environment in Linux.

  - `./VirusSimulator --epicurve`

## 3.0.5 Run with MPI execution
Due to the new optimisation, running over MPI execution does not require execution optimisation.
However, you need to use MPI application executor to run the simulation to gain MPI functionality.

If you use OpenMPI, you will have `mpirun` available in your system when OpenMPI has been installed.

To execute MPI:

- `mpirun -np 4 ./VirusSimulator`

Where 4 means 4 available node / core for your execution.