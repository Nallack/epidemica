# 2 Build {#build-overall}
This section will go through the guide to build this simulator.

## 2.0.1 Build status
This project has been tested on the following operating system:

- GNU Linux System (Debian, Ubuntu)
- RPM based system (RedHat Enterprise Linux, CentOS)
- Windows MSVC
- Windows MinGW
- OSX

## 2.0.2 Pre-required Skills
The following skilled assumed to known by the user for installation and execution of this simulation.
- Ability of using command line interface
- Knowledge of basic POSIX file structure
- Knowledge of basic file operations over command line interface

## 2.0.3 System Environment Requirement
The following base software are required to set environment for Zika Virus Simulation.

### Git
Git is free and open source distributed version control system. It can handle small to large project with speed and efficiency. Git comes with build-in GUI tools for committing (git-gui) and browsing (gitk). We need to download the required git setup according to our system.
 
The following platform of Git: Gitkraken, gitg, ungit, GitEye, giggle, SmartGit, git-cola, Cycligent Git Tod, GitAhead, CodeReview.

The minimum requirement of git version, in order to make use of this project is Git 1.7 or above.

### CMake
CMake is cross platform free and open-source software which used to manage the build process of software. This uses a compiler-independent method. CMake is upstream repository. CMake can be built for various platforms. Contribution to CMake development can be done by creating an account on Github.

Based on CMake functionality used, the minimum version of CMake is 3.2 or above.

### OpenMP (Open multi-processing)
OpenMP is an application program interface (API), jointly defined by a group of major computer and hardware and software vendors. This also support multi platform shared memory multiprocessing programming in C, C++ etc.

### MPI
This is a message passing interface. It is a specification for the developers and users of message passing libraries. It is not a library but is a specification. It also addresses the message-passing parallel programming model.

We recommended to use OpenMPI for MPI functionality, however generally every MPI implementation is useable. Please read the corresponding manual for further MPI implementation.

### Compiler Essential packages
A combination of code compiler. In particular, this includes C++ compiler and makefiles.

Due to the backward-compatiability and implementation diversity between GCC versions,
we cannot guarantee the code can be compiled over all different GCC versions.

According to our test result, the following GCC version is able to compile this project:

- gcc/4.9.2
- gcc/5.3
- gcc/5.2
- gcc/6.2

## 2.0.4 Thrid-Party Libraries
This virus simulator relay on multiple third-party modules, including easylogging++, GTest framework, MatPlot
Library for C++ and rapidjson. These modules, by default, not loaded in this project folder.

You need to perform manual actions to include those modules. This will be further explained in later sections.
