# 1 introduction {#intro}

Arbovirus C++ Simulator, the simulator emulating real-world vector-brone transmitting diseases.

Originally created by Software Modelling Team, from the University of Western Australia. 
Translated by Team Culico for high performance computing compatiability.

Architecture optimised for extenability, enable future developers
to modify processes, pipeline, implementation as desire.

Team Member:
- Project Owner:
  - Prof. George Milne
  - Dr. Joel Kelso
- Project Supervisor:
  - Tim French
- Project Team: (Team Culico)
  - Callan Gray - Student Number 21341958
  - Jiaojie Wei (George) - Student Number 21553103
  - Prerna Toppo - Student Number 21996929
  - Qiang Sun (Pascal) - Student Number 21804416
  - Zhisheng Xie (Simon) - Student Number 21259203

## 1.1 Basic Features
* High performance optimisation using OpenMP
* cross-platform supported by CMake
* Multi-Instance execution by MPI

## 1.2 Known issues

As the incompatiability between Doxygen and any other MarkDown extension, table of contents
has been duplicated for maximum compatiability. However, when reading markdown from Doxygen
the in-page table of contents will become non-functional.

## 1.3 Special Thanks

- the University of Western Australia, for providing project
- Pawsey Supercomputing center, for providing supercomputing resource and infrastructure