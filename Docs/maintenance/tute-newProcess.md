# 5 Guideline for Extend program {#tute-newProcess}
As described before, this program is available to be extended, by adding new process, re-order or totally create a new simulation process.
In this section, you will learn the basic thundermental to extend this function.

## 5.1 Add new process into model.
We starting with the single threaded code. the process is:
- create a new C++ and header file within single threaded folder
- link the new process into Process Factory
- Add the process together into Abstract Process Factory.
- add the new process into Device Manager.

## 5.2 Ultilize the process from single thread to high performance
This will be more effort than just write the algorithm. As when you are trying
to make your program into high performance computing, you need to understand
the reason why it run slow and think about the best strategy for ultilizing it.
The pattern for doing this is:
- Copy the algorithm from single thread to the optimisation implementation of your choice
- implement the optimisation by implementation manual
- run the process, test the performance, edit for higher performance.

## 5.3 create new process pipeline
When you need to fit a new model, or you want to think about a new process pipeline,
all you need to do is put all process by your order into VirusFramework -> DeviceManager

You can re-order the existing pipeline, together you can create a new one.
But after you created, make sure you placed the program name into header
and modify the VirusSimulator->main to fit your new process pipeline.