# 4.1 System Architecture {#architecture}

This simulator are pretented to be performed by the c++ class diagram outlined below:

(@ref cpp-class.png)
![C++ class diagram](cpp-class.png)
(if image not shown, it is located at Docs/maintenance/cpp-class.png)

Currently, this project are constructed by the following parts:

## VirusFramework

The virusFramework's structure are located below:
(@ref virusFramework.png)
![structure of virusFramework](virusFramework.png)

In general, each implementation has their process factory, where ou can edit,
add or remove. You are available to add in any algorithm of your choice within
any of the processFactory and execute with any order.

## VirusSimulator

virusSimuator is the front-end controller for virusFramework, where it communicates
with the framework to provide scenario information.

In another word, it is the controller and the interface between user and the core.