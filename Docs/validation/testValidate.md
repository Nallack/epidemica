# 6 Testing And Validation {#testValidate}

## 6.1 Testing

All testing is set up in the VirusFrameworkTests project. These are for the purpose of ensuring that the library is running as expected.

Writing more test cases can be beneficial for assisting future development, but excessive amounts may result in extended efforts in  updating test suits to accomodate for changes and new features. 

A general set of recommended guideline for test writing are as follows:

- Primarily aim to test features from a high level of abstraction with deep call stacks rather than individual functions and methods.
- Write tests that checks features from a user perspective as much as possible as this typically can be asserted much more easily.
- When finding bugs, create a test fixture that checks for its symptoms until it is resolved. This will help future devs know whether they have reintroduced the issue.

## 6.2 GTest

GTest is a widely popular unit testing framework developed by Google that is capable of controlling the execution of test suites from a single executable. Samples and documentation for writing and executing gtests is available here: https://github.com/google/googletest

The VirusFrameworkTests project is a GTest project that builds and links to VirusFramework project and contains all of the automated tests suites.

## 6.3 Test Summaries

The tests in this project have been broken down into the following test suits:

**JSONScenarioTests**

  - Tests the loading saving of JSONScenario objects. These tests are to confirm that scenario files are loading correctly.

**ProcessPipelineTests**

  - Tests for whether the expected process pipelines can be created.
  - At the time of writing this includes the single threaded and the openmp implementation pipelines.

**ArbovirusSystemTests**

  - Test suite that confirms that the running of JSON scenarios with the arbovirus pipeline is functional and is consistant in single threaded mode.

Documentation regarding each test is available in the respective source files and doxygen.

## 6.4 Data Validation
Data validation with the original python simulation can be done by using the .log files generated with the provided python tool.

The tool repo link: [https://bitbucket.org/grouplearn/design-visual](https://bitbucket.org/grouplearn/design-visual)

The validation tools are divided into 2 parts: statistics tools and visualization tool. 

To do the validation, follow the steps:

- Run the same scenario with the same location, json and param using original python code, the log will stored in output.csv
- Analyze python output
    - copy the output.csv file into Validation folder, and edit the validate.py to read the file, then run with ``python3 validate.py``
    - copy the output.csv file and location/host file into Visualization fold, and edit visual.py to read the file, then run with ``python3 visual.py``
    - The program should output with 3 graphs altogether with fomulas, and a dynamic plot of the simulation process. Store it
- Analyze cpp logs
    - copy *.log from server to the Validation folder, and run ``python3 transfer``, it will transfer the log files into the format which is the same as python output.
    - then repeat the process above
- Validation 
    - Compare the graphs to check whether there have signaficance differences 
    - Compare the dynamic plot to check whether the process is in the same pace
