cmake_minimum_required(VERSION 3.2 FATAL_ERROR)

#already included
#include(../cmake/AutoSourceGroup.cmake)
#include(../cmake/PrecompiledHeader.cmake)

set(TARGET_NAME VirusFramework)

#==========================
# Source Files
#==========================
file(
	GLOB_RECURSE CXX_SRCS
	LIST_DIRECTORIES false
	RELATIVE ${CMAKE_CURRENT_LIST_DIR}
	"${CMAKE_CURRENT_LIST_DIR}/*.cpp"
)

file(
	GLOB_RECURSE CXX_HEADERS
	LIST_DIRECTORIES false
	RELATIVE ${CMAKE_CURRENT_LIST_DIR}
	"${CMAKE_CURRENT_LIST_DIR}/*.h"
)

#========================
# MatplotLib
#========================
include_directories(../third_party/)

#add_library(VirusFramework SHARED ${CXX_SRCS})
add_library(${TARGET_NAME} ${CXX_SRCS} ${CXX_HEADERS})

#source_group(TREE FILES ${CXX_SRCS})
#auto_source_group(${CXX_SRCS})


#auto_filter_group("Header Files" ${CXX_HEADERS})
#auto_filter_group("Source Files" ${CXX_SRCS})
auto_rel_filter_group("Code" include/epidemica ${CXX_HEADERS})
auto_rel_filter_group("Code" src ${CXX_SRCS})

# Properties->C/C++->General->Additional Include Directories
include_directories(
    .
	pch
	include
	include/epidemica)


#=========================
# Library Configuration
#=========================
target_include_directories(${TARGET_NAME} PUBLIC ${INCLUDE_DIRECTORIES})
target_link_libraries(${TARGET_NAME} ${LINK_OPTIONS})

# Creates folder "libraries" and adds target project (math.vcproj)
set_property(TARGET ${TARGET_NAME} PROPERTY FOLDER "libraries")

# Properties->General->Output Directory
set_target_properties(${TARGET_NAME} PROPERTIES
		RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)

# from https://github.com/larsch/cmake-precompiled-header/blob/master/example/CMakeLists.txt
# could alternatively rename "stdafx" to "pch"
if(MSVC)
add_precompiled_header(
		${TARGET_NAME}
		"pch/stdafx.h"
		FORCEINCLUDE
		SOURCE_CXX "src/stdafx.cpp")
endif()

install (TARGETS ${TARGET_NAME}
		 ARCHIVE DESTINATION ${PROJECT_SOURCE_DIR}/_install
		 RUNTIME DESTINATION ${PROJECT_SOURCE_DIR}/_install
		 LIBRARY DESTINATION ${PROJECT_SOURCE_DIR}/_install)
