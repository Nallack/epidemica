#pragma once

#include <epidemica/state/VectorState.h>

#include <vector>
#include <memory>


namespace Epidemica
{
    class DeviceManager;
    struct SimulationState;
    class PTreeSimulationScenario;
    class ISimulationScenario;
    class ProcessPipeline;
    class AbstractProcessFactory;

    /**
     * @brief The simulation container that manages devices, simulation data, processes, and execution of the simulation.
     *        Accessors and mutators of this class will directly modify only the simulation state, and will only be stored to simulation
     *        scenarion after calling SaveStateToScenario.
     */
    class Simulation
    {
    private:
        std::shared_ptr<DeviceManager> m_deviceManager; // object that stores all factory/device objects of HPC libraries.
        std::shared_ptr<ISimulationScenario> m_scenario; // the simulation scenario, overriden each time a checkpoint is made.
        std::shared_ptr<ProcessPipeline> m_processes;   // the processes that define how the simulation runs.
        std::shared_ptr<SimulationState> m_state;       // the simulation state structure, avoid use of maps/dictionary. 
        std::vector<seir_vector> m_hostsEpidemic;       // tracking of epidemic curve, could aim for a more modular approach to this.

        //TODO: consider splitting devices and factories apart as some factories are hybrid.

        //int m_currentStep = 0;
        //int m_totalSteps = 0;
        
    public:

#pragma region Construction
        /**
         * @brief Constructor.
         * @param factoryTag   The factory tag.
         * @param scenarioFile The scenario file.
         *
         * ### remarks in case you are wondering when to pass smart pointers, only pass a smart pointer
         *             when the receiving object is expected to be responsible for ownership/disposing, otherwise
         *             use reference.
         */
        Simulation(const std::string& factoryTag, const std::string& scenarioFile);

        /**
         * @brief Constructor.
         * @param factoryTag    The factory tag.
         * @param deviceManager Manager for device.
         */
        Simulation(const std::string& factoryTag, std::shared_ptr<DeviceManager> deviceManager);

        /**
         * @brief Constructor.
         * @param factoryTag    The factory tag.
         * @param deviceManager Manager for device.
         * @param scenario      The scenario.
         */
        Simulation(const std::string& factoryTag, std::shared_ptr<DeviceManager> deviceManager, std::shared_ptr<ISimulationScenario> scenario);

        /**
         * @brief Constructor.
         * @param factoryTag      The factory tag.
         * @param deviceManager   Manager for device.
         * @param scenario        The scenario.
         * @param processPipeline The process pipeline.
         */
        Simulation(
            const std::string& factoryTag,
            std::shared_ptr<DeviceManager> deviceManager,
            std::shared_ptr<ISimulationScenario> scenario,
            std::shared_ptr<ProcessPipeline> processPipeline);

        /**
         * @brief Destructor.
         */
        ~Simulation();

#pragma endregion Construction

#pragma region Accessors_Mutators
        /**
         * @brief Query if this object is done.
         * @return True if done, false if not.
         */
        bool IsDone() const;

        /**
         * @brief Query if the current cycle is a checkpoint
         */
        bool IsCheckpoint() const;

        /**
         * @brief Sets a scenario.
         * @param scenario The scenario.
         */
        void SetScenario(std::shared_ptr<ISimulationScenario> scenario) { m_scenario = scenario; }

        /**
         * Gets the scenario
         * @return The scenario.
         */
        ISimulationScenario& GetScenario() const { return *m_scenario; }

        /**
         * @brief Sets a state.
         * @param state The state.
         */
        void SetState(const SimulationState& state) { m_state = std::make_unique<SimulationState>(state); }

        /**
        * @brief Gets the state.
        * @return The state.
        */
        SimulationState& GetState() const { return *m_state; }

        /**
        * @brief Resets the current simulation state to the given values.
        * @param steps The steps.
        */
        void ResetSteps(int currentSteps, int totalSteps);

        /**
        * @brief sets the checkpoint interval
        * @param interval number of cycles between checkpoints
        */
        void SetCheckpointInterval(int interval);

        /**
        * @brief sets the checkpoint interval
        * @param interval number of cycles between checkpoints
        */
        int GetCheckpointInterval() const;

        /**
         * @brief Gets hosts epidemic.
         * @return The hosts epidemic.
         */
        std::vector<seir_vector>& GetHostsEpidemic() { return m_hostsEpidemic; }


#pragma endregion Accessors_Mutators

        //SCENARIO METHODS


        /**
         * @brief Loads the current scenario files into the simulation state
         */
        void LoadStateFromScenario();

        /**
         * Saves the current scenario to the same input files
         */
        void SaveStateToScenario();

        /**
         * @brief Saves the current scenario to the input scenario file
         */
        void SaveScenarioToInputFile();

        /**
        * @brief Saves the current scenario to checkpoint file
        */
        void SaveScenarioToCheckpointFile();


        //EXECUTION methods


        /**
         * @brief Initializes this object. Some factories such as MPI requires an initialize
         *        call to be made.
         * @param steps The steps.
         */
        void Initialize();

        /**
         * @brief Finalizes this object.
         */
        void Finalize();

        /**
         * @brief Run simulation for a predetermined number steps (store cycle and remaining_cycles in
         *        simstate)
         */
        void Run();
        
        /**
         * @brief Steps the simulation by running the complete process pipeline once.
         */
        void Step();

        /**
         * @brief Stream insertion operator.
         * @param [in,out] strm The stream.
         * @param          a    A Simulation to process.
         * @return The shifted result.
         */
        friend std::ostream& operator<<(std::ostream& strm, const Simulation& a);
    };
}
