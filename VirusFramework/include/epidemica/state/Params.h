#pragma once

#if RAPIDJSON_FOUND
#include <rapidjson/document.h>
#endif
#if BOOST_FOUND
#include <boost/property_tree/ptree.hpp>
#endif

namespace Epidemica
{
    /**
     * @brief Contains a collection of parameters/configurable constants to be used
     *        throughout a simulation.
     * \remarks It may be desirable to store this data in a property tree state to allow for
     *          extensions in future, otherwise there should not be any harm in adding additional
     *          members that may go unused. 
     */
    struct Params
    {
    public:
        double vectorDiffusionProbability;
        double bitingRate;
        double vectorInfectionProbability;
        double hostInfectionProbability;
        double vectorEIRate;
        double hostEIRate;
        double hostIRRate;
        double vectorBirthRate;
        double vectorMaturationRate;
        double vectorDeathRate;

        /**
         * @brief Default constructor.
         */
        Params();

#if BOOST_FOUND
        /**
         * @brief Constructor using a property tree object.
         * @param params param property tree.
         */
        Params(const boost::property_tree::ptree& params);
#endif

#if RAPIDJSON_FOUND
        Params(const rapidjson::GenericObject<true, rapidjson::Value>& params);
        void Load(const rapidjson::GenericObject<true, rapidjson::Value>& params);
        void Save(const rapidjson::GenericObject<false, rapidjson::Value>& params, rapidjson::MemoryPoolAllocator<>& allocator) const;
#endif

        /**
         * @brief Destructor.
         */
        ~Params();

    };
}