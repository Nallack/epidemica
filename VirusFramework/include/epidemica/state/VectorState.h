#pragma once

#include <epidemica/Types.h>
#include <string>

namespace Epidemica
{
    const int INFECTION_STATES = 4; //Total number of infectionstates for aggregate containers

    /**
     * \enum InfectionState
     * @brief Values that represent infection states.
     */
    enum InfectionState
    {
        S = 0, // Susceptible
        E = 1, // Exposed
        I = 2, // Infectious
        R = 3  // Recovered
    };

    /**
     * @brief Converts a string to infection state
     * @param character The character.
     * @return An InfectionState.
     */
    InfectionState StringToInfectionState(const char* character);

    /**
     * @brief Converts an infection state to a string
     * @param state The state.
     * @return A std::string.
     */
    std::string InfectionStateToString(InfectionState state);
    
    /**
     * \array infection event abbreviation
     * @brief Values that represent infection event name.
     */
    static char eventAbrreviation[] = {'s','e','i','r'};

    /**
     * @brief Contains the vectors data for a Location such as
     *        vector popululation for 
     */
    struct VectorState
    {

        int Capacity; // The maxmimum vector capacity
        int Immature; // Population of immature vectors that cannot transfer yet
        seir_vector SEIR; // Contains vector population of each infection state

        /**
         * @brief Default constructor.
         */
        VectorState() : 
            Capacity(0),
            Immature(0),
            SEIR()
        {
        }
    };
}

