#pragma once

#if RAPIDJSON_FOUND
#include <rapidjson/document.h>
#endif
#if BOOST_FOUND
#include <boost/property_tree/ptree.hpp>
#endif

namespace Epidemica
{
    enum CheckpointMode
    {
        NONE,
        CYCLES,
        REALTIME,
        UNKNOWN
    };

    /**
     * @brief A collection of simulation runtime properties that do not affect
     *        do not represent parameters in natural science, but how a simulation
     *        will be run.
     */
    struct Config
    {
    public:
        long CurrentCycle;
        long TotalCycles;

        CheckpointMode Mode;
        long CheckpointInterval;

        unsigned int Seed;

        /**
         * @brief Default constructor
         */
        Config();

        /**
         * @brief Destructor
         */
        ~Config();

#if RAPIDJSON_FOUND
        Config(const rapidjson::GenericObject<true, rapidjson::Value>& config);

        /**
         * @brief Loads the config from a RapidJSON object.
         * @param config The configuration to load.
         */
        void Load(const rapidjson::GenericObject<true, rapidjson::Value>& config);

        /**
         * @brief Saves the config to a RapidJSON object.
         * @param          config    The configuration.
         * @param [in,out] allocator The allocator.
         */
        void Save(const rapidjson::GenericObject<false, rapidjson::Value>& config, rapidjson::MemoryPoolAllocator<>& allocator) const;
#endif
    };

}
