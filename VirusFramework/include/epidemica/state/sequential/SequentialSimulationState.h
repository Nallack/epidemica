#pragma once

#include <epidemica/state/VectorState.h>
#include <epidemica/state/Params.h>

#include <epidemica/Types.h>
#include <vector>

namespace Epidemica
{
    namespace Sequential
    {
        class ISimulationScenario;
        struct Host;
        struct Location;

        /**
         * Experimental SimulationState
         * 
         * Cache-hit optimisation can be acheived by storing data close together based on the order most
         * operations typically access it, not necessarily as complete objects. An example is
         * InfectionInHosts and InfectionInVectors, where iteration only cares about the InfectionState
         * and VectorState array attributes.
         * 
         * This approach may have significant downsides, as some processes need to access multiple
         * attributes, which could have much worse memory thrashing, but may still be viable if we
         * carefully modify the structure of other processes to only access a single array attribute per
         * loop.
         * 
         * Sequential SimulationState groups all attributes together in lists, that is all arrays should
         * be of primitive types where possible.
         */
        struct SimulationState
        {
        public:

            long CurrentCycle;
            long m_totalCycles;

            //params
            Params m_params;

            //hosts
            vector<int> m_hostsIds;
            vector<int> m_hostsLocationId;
            vector<InfectionState> m_hostsInfectionState;
            vector<int> m_hostsHomeId;
            vector<int> m_hostsHubId;
            
            //locations
            vector<int> m_locationsId;
            vector<int> m_locationsXcoord;
            vector<int> m_locationsYcoord;
            vector<VectorState> m_locationsVectorState;
            vector<neighbour_vector> m_locationsNeighbours;
            vector<host_set> m_locationsHosts;

            /**
             * Default constructor
             */
            SimulationState();

            /**
             * Constructor
             * @param scenario The scenario.
             */
            SimulationState(const ISimulationScenario& scenario);

            /**
             * Destructor
             */
            ~SimulationState();

            /**
             * Gets hosts epidemic vector.
             * @return The hosts epidemic.
             */
            seir_vector GetHostsEpidemic() const;

            /**
             * Query if the simulation is day time
             * @return True if day time, false if not.
             */
            bool IsDayTime();

            //friend std::ostream& operator<<(std::ostream & stm, const Epidemica::Sequential::SimulationState& a);
        };
    }
}
