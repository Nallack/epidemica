#pragma once

#include <epidemica/Types.h>

namespace Epidemica
{
    struct Host;

    /**
     * @struct	HostCollection
     *
     * @brief	Collection of hosts. not implemented, This can be implemented as LocationCollection.
     */

    struct HostCollection
    {
    public:
        vector<Host> Collection;



    };

}