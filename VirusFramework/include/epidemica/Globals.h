#pragma once

#include <vector>
#include <string>
#include <epidemica/profiling/TimeProfiler.h>
#include <epidemica/logger/Logger.h>

//put in the exception library into here
#include <stdexcept>

namespace Epidemica
{
    /**
     * @brief a class containing global objects similarly to using a singleton pattern.
     *        Multiple get instance methods exist to avoid the need to introduce the
     *        singleton pattern to pure object oriented code.
     */
    class Globals
    {
    private:
        /**
         * Default constructor
         * making this constructor private prevents other code from creating instances of this class.
         */
        Globals() {}

        /**
         * Destructor
         */
        ~Globals() {}

        /**
         * @brief Copy constructor.
         */
        Globals(Globals const&) = delete;

        /**
         * @brief Assignment operator.
         */
        void operator=(Globals const&) = delete;

    public:
        static const std::vector<std::string> FALLBACKTAGS;

        /**
         * @brief Gets time profiler.
         * @return The time profiler.
         */
        static TimeProfiler* GetTimeProfiler()
        {
            static TimeProfiler s_instance;
            return &s_instance;
        }

        /**
         *
         * @brief [legacy] Gets Logger.
         * @return The time profiler.
         */
        //static STLogger* GetLogger()
        //{
        //    static STLogger s_instance;
        //    return &s_instance;
        //}
    };
}