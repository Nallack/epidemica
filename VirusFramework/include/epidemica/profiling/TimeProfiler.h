#pragma once

#include <map>

namespace Epidemica
{
    /**
     * @brief A time profiler.
     * 
     * TODO: consider storing a stopwatch here as well that can be queried to measure
     * how much other time is wasted.
     */
    class TimeProfiler
    {
    private:
        double m_timeElapsed;
        std::map<std::string, double> m_timeMetrics;

    public:
        /**
         * @brief Default constructor.
         */
        TimeProfiler()
		{ 
			m_timeMetrics = std::map<std::string, double>();
		};

        /**
         * @brief Destructor.
         */
        ~TimeProfiler() { }

        std::map<std::string, double>* GetMetrics()
        {
            return &m_timeMetrics;
        }

        double GetTime(const std::string& name) const
        {
            return m_timeMetrics.at(name);
        }

        double GetTotalTime() const
        {
            return m_timeElapsed;
        }

        /**
        * @brief Gets time factor.
        * @param name The name.
        * @return The time factor.
        */
        double GetTimeFactor(const std::string& name) const
        {
            return m_timeMetrics.at(name) / m_timeElapsed;
        }

        /**
         * @brief Adds a time factor to 'time'.
         * @param name The name.
         * @param time The time.
         * @return A double.
         */
        void AddTime(const std::string& name, double time)
        {
            auto it = m_timeMetrics.find(name);
            if (it == m_timeMetrics.end())
            {
                //emplace new entry
                m_timeMetrics[name] = time;
            }
            else
            {
                //increment existing entry
                m_timeMetrics[name] += time;
            }
            m_timeElapsed += time;
        }


    };
}

