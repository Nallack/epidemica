#pragma once

namespace Epidemica
{
    /**
     * @class	IStopwatch
     *
     * @brief	A stopwatch algorithm provided for programm profiling - how much time consumed.
     */

    class IStopwatch
    {
    public:
        //template<typename T>
        //virtual T GetTime() const = 0;

        virtual void Start() = 0;
        virtual void Stop() = 0;
        virtual void Reset() = 0;
        virtual void Restart() = 0;
    };
}