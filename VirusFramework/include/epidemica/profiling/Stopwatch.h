#pragma once

#include <chrono>
#include <epidemica/profiling/IStopwatch.h>

using namespace std::chrono;

namespace Epidemica
{
    /**
     * @brief A stopwatch implementation using the standard chrono library.
     */
    class Stopwatch : IStopwatch
    {
    private:
        std::chrono::steady_clock::time_point m_start;
        std::chrono::steady_clock::time_point m_end;

    public:

        template<typename T>
        T GetTime() const
        {
            if (m_end == steady_clock::time_point::min())
            {
                std::chrono::duration<T> time_span =
                    std::chrono::duration_cast<std::chrono::duration<T>>(steady_clock::now() - m_start);
                return time_span.count();
            }
            else
            {
                std::chrono::duration<T> time_span =
                    std::chrono::duration_cast<std::chrono::duration<T>>(m_end - m_start);
                return time_span.count();
            }
        }

        void Start() override
        {
            m_start = steady_clock::now();
        }

        void Stop() override
        {
            m_end = steady_clock::now();
        }

        void Reset()
        {
            m_end = steady_clock::time_point::min();
            m_start = steady_clock::time_point::min();
        }

        void Restart()
        {
            m_end = steady_clock::time_point::min();
            m_start = steady_clock::now();
        }
    };
}
