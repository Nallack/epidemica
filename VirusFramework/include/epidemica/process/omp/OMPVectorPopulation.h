#pragma once
#if OPENMP_FOUND
#include <epidemica/process/IProcess.h>

#include <epidemica/Types.h>
#include <random>

namespace Epidemica
{
    namespace OMP
    {
		/**
		 * @class	OMPVectorPopulation
		 *
		 * @brief	An OpenMP used vector population process.
		 */

		class OMPVectorPopulation : public IProcess
        {
        private:
            random_engine m_generator;
        public:
            OMPVectorPopulation();
            ~OMPVectorPopulation();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;
        };
    }
}
#endif
