#pragma once
#ifdef OPENMP_FOUND
#include <epidemica/process/IProcess.h>
#include <random>

namespace Epidemica
{
    namespace OMP
    {	
        /**
         * @class	OMPInfectionInHosts
         *
         * @brief	An OpenMP used infection in hosts process.
         *
         */

        class OMPInfectionInHosts : public IProcess
        {
        private:
            std::default_random_engine m_generator;
        public:
            OMPInfectionInHosts();
            ~OMPInfectionInHosts();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;
        };
    }
}
#endif