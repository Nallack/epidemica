#include <epidemica/process/omp/OMPInfectionInHosts.h>
#include <epidemica/process/omp/OMPInfectionInVectors.h>

#include <epidemica/process/omp/OMPVectorMovement.h>
#include <epidemica/process/omp/OMPRegularHumanMovement.h>

#include <epidemica/process/omp/OMPVectorInfection.h>
#include <epidemica/process/omp/OMPHostInfection.h>

#include <epidemica/process/omp/OMPVectorPopulation.h>