#pragma once
#if OPENMP_FOUND
#include "epidemica/process/AbstractProcessFactory.h"

namespace Epidemica
{
    class IProcess;

	/**
	 * @class	OpenMPProcessFactory
	 *
	 * @brief	An open mp process factory, extendable by implementing your own processes.
	 * 
	 * In particular, this class is for you to organise the order, add or remove any process of your simulation.
	 * 
	 * Please read through maintainance manual for guideline of modification.
	 * 
	 * @author Callan
	 */

	class OpenMPProcessFactory : public AbstractProcessFactory
	{
	public:
        /**
         * Default constructor
         */
        OpenMPProcessFactory();

        /**
         * Destructor
         */
        ~OpenMPProcessFactory();

        /**
         * Gets the tag
         * @return A std::string.
         */
        static std::string tag() { return "OMP"; }

        /**
         * Creates the process from a name
         * [not implemented]
         * @param processName Name of the process.
         * @return Null if it fails, else the new process.
         */
        IProcess* CreateProcess(std::string processName) override;

        virtual IProcess* CreateInfectionInVectors() override;
        virtual IProcess* CreateInfectionInHosts() override;
        virtual IProcess* CreateRegularHumanMovement() override;
        virtual IProcess* CreateVectorMovement() override;
        virtual IProcess* CreateVectorInfection() override;
        virtual IProcess* CreateHostInfection() override;
        virtual IProcess* CreateVectorPopulation() override;
	};
}

#endif