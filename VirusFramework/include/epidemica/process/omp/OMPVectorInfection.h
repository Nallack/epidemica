#pragma once
#if OPENMP_FOUND
#include <epidemica/process/IProcess.h>

#include <random>

namespace Epidemica
{
    namespace OMP
    {
        /**
         * @class	OMPVectorInfection
         *
         * @brief	An OpenMP used vector infection process.
         */

        class OMPVectorInfection : public IProcess
        {
        private:
            std::default_random_engine m_generator;
        public:
            OMPVectorInfection();
            ~OMPVectorInfection();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;
        };
    }
}
#endif