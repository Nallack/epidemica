#pragma once
#include <epidemica/process/IProcess.h>

#include <epidemica/Types.h>
#include <random>

namespace Epidemica
{
    struct SimulationState;

    namespace MPI
    {
		/**
		 * @class	MPIInfectionInVectors
		 *
		 * @brief	The Infection In Vector Process in the model, duplicated from Python to MPI.
		 *
		 */

		class MPIInfectionInVectors : public IProcess
        {
        private:
            random_engine m_generator;

        public:
            MPIInfectionInVectors();
            ~MPIInfectionInVectors();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;
        };
    }
}

