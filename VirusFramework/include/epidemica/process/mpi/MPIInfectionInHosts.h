#pragma once
#include <epidemica/process/IProcess.h>
#include <random>

namespace Epidemica
{

    namespace MPI
    {
        /**
         * @class	MPIInfectionInHosts
         *
         * @brief	The Infection In Hosts process, used for MPI.
         *
         */

        class MPIInfectionInHosts : public IProcess
        {
        private:
            std::default_random_engine m_generator;
        public:
            MPIInfectionInHosts();
            ~MPIInfectionInHosts();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;
        };
    }
}
