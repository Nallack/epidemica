#pragma once
#include <epidemica/process/IProcess.h>

#include <epidemica/Types.h>
#include <random>

namespace Epidemica
{
    struct SimulationState;

    namespace ST
    {
        /**
         * @class	STInfectionInVectors
         *
         * @brief	The infection happened in vectors. Where changing stage of SEIR for vector.
         */

        class STInfectionInVectors : public IProcess
        {
        private:
            random_engine m_generator;

        public:
            STInfectionInVectors();
            ~STInfectionInVectors();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;
        };
    }
}

