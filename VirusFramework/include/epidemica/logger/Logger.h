#pragma once

#include <iostream>

namespace Epidemica
{
    /**
     * @class	ILogger
     *
     * @brief	The logging header. Contains logging strategy.
     */

    class ILogger
    {
    public:
        /**/
        virtual std::ostream& GetStream() = 0;
        virtual void Write(const std::string&) = 0;
        virtual void Flush() = 0;
    };

    /**
    * @brief Logging class that will immediately write to a file.
    */
    class STLogger : ILogger
    {
    private:
        std::ostream& m_stream;

    public:
        STLogger() : STLogger(std::cout) { }
        STLogger(std::ostream& stream) : m_stream(stream) { }
        ~STLogger() { }

        std::ostream& GetStream() { return m_stream; }
        void Write(const std::string& message)
        {
            m_stream << message;
        }
        void Flush()
        {
            m_stream.flush();
        }


    };

    /**
     * @brief Logging class that can be merged with other threads
     * using timestamps and sorted before giving final output.
    class MTLogger
    {
    public:
        MTLogger();
        ~MTLogger();
    };
    */
}

