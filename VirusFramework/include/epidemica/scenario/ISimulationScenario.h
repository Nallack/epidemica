#pragma once

#include <string>

namespace Epidemica
{
    struct SimulationState;

    /**
     * @brief Interface for a simulation scenario. Scenarios are an intermediate
     * class that are capable of storing information about where and how a simulation
     * should be saved to a file system. Potential scenario implementations include:
     * - RapidJSONSimulationScenario
     * - PTreeSimulationScenario
     * - HDF5SimulationScenario
     * - CSVSimulationScenario
     */
    class ISimulationScenario
    {
    public:
        /**
         * Gets location where this scenario was loaded from.
         */
        virtual const std::string& GetFileLocation() = 0;

        /*
         * @brief Read specified file from storage into this scenario
         */
        virtual void LoadScenarioFromFile() = 0;

        /*
         * @brief read specified file from storage into this scenario
         */
        virtual void LoadScenarioFromFile(const std::string& configpath) = 0;

        /**
         * Saves the scenario to file
         */
        virtual void SaveScenarioToInputFile() = 0;

        /*
         * @brief Saves this scenario to a file
         */
        virtual void SaveScenarioToFile(const std::string& name, const std::string& path) = 0;

        /*
         * @brief Saves this scenario to a file with a checkpoint timestamp
         */
        virtual void SaveScenarioToCheckpointFile() = 0;

        /**
         * @brief Moves data from a scenario into a simulation state
         */
        virtual void LoadState(SimulationState& state) const = 0;

        /**
         * @brief Saves a simulation state back into a scenario for writing a checkpoint
         */
        virtual void SaveState(const SimulationState& state) = 0;
    };
}

