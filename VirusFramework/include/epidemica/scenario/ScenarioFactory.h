#pragma once

#include <string>

#include "epidemica/scenario/ISimulationScenario.h"

namespace Epidemica
{
    struct SimulationState;
    class PTreeSimulationScenario;
    class JSONSimulationScenario;

    /**
    * @brief Factory class for converting simulations between formats.
    */
    class ScenarioFactory
    {
    public:        
        static ISimulationScenario* MakeDynamicSimulationScenarioFromJSONFile(const std::string& filepath);
        static SimulationState* MakeStateFromJSONScenario(const ISimulationScenario& scenario);


        //STATE FACTORY (Unused)
        //static SimulationState* MakeStateFromJSONFile(std::string filepath);
        //static SimulationState* MakeStateFromHDF5(std::string filepath);
    };
}