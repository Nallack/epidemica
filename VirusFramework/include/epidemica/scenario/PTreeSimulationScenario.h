#pragma once
#ifdef BOOST_FOUND

#include <epidemica/scenario/ISimulationScenario.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/filesystem/path.hpp>

namespace Epidemica
{
    /**
    * @class PTreeSimulationScenario
    * @date 01/08/2017
    * @brief We could probably make do with just the ptree instead of this class,
    * but there are a lot of default parameters that need to be allocated.
    * This is the equivalent of the SimulationConfig from python code, but here we can afford
    * keep the unmodified values of config, hosts, location json files.
    */
    class PTreeSimulationScenario : public ISimulationScenario
    {
    private:
        //config
        std::string m_configfile;
        boost::property_tree::ptree m_config;

        //params
        std::string m_paramsfile;
        boost::property_tree::ptree m_params;

        //hosts
        std::string m_hostsfile;
        boost::property_tree::ptree m_hosts;

        //locations
        std::string m_locationsfile;
        boost::property_tree::ptree m_locations;

        //datalog
        std::string m_datalogfile;

    public:

        //TODO: potentially allow for legacy scenarios, and a new type containing
        // hosts, location, params, and config, where config contains instructions
        // regarding how to execute, while params contains constants about the simulation

        PTreeSimulationScenario();
        PTreeSimulationScenario(std::string filename);
        ~PTreeSimulationScenario();

        void LoadState(SimulationState& state) const override;
        void SaveState(const SimulationState& state) override;
        void SaveStateCheckpoint(const SimulationState& state) override;

        const boost::property_tree::ptree& GetConfig() const { return m_config; }
        const boost::property_tree::ptree& GetHosts() const { return m_hosts; }
        const boost::property_tree::ptree& GetLocations() const { return m_locations; }
        const boost::property_tree::ptree& GetParams() const { return m_params; }

        /**
        * @brief Loads the params, hosts and locations file defined in the config to a property tree.
        * @param filename  path to the config file.
        */
        void LoadScenario(const std::string& filename);
        
        /**
        * @brief Loads the params, hosts and locations file defined in the config to a property tree.
        * @param config    The configuration.
        * @param directory Pathname of the directory.
        */
        void SaveScenario(const std::string& filename);

    private:


        /**
        * @brief Loads the params, hosts and locations file defined in the config to a property tree.
        * @param config    The configuration.
        * @param directory Pathname of the directory.
        */
        //void LoadScenario(boost::property_tree::ptree config, boost::filesystem::path directory);
    };
}
#endif