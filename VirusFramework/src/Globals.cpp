#include <stdafx.h>

#include <Globals.h>

/**
 * @brief The epidemica globals fallbacktags
 */
const std::vector<std::string> Epidemica::Globals::FALLBACKTAGS = 
{ 
    //"OMPI", 
    //"MPI", 
    "OMP", 
    "ST"
};