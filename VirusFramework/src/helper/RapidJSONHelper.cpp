
#define RAPIDJSON_HAS_STDSTRING 1

#include "epidemica/helper/RapidJSONHelper.h"
#include "epidemica/state/Host.h"
#include "epidemica/state/Location.h"


#include <rapidjson/rapidjson.h>
#include <rapidjson/encodings.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include <memory>

//Maximum length of the json index
// (This probably should be an integer in JSON, but
// we must parse it in some way instead)
#define MAX_INDEX_LENGTH 100

namespace Epidemica
{
    void RapidJSONHelper::SaveLocations(
        const vector<Location>& locations,
        const rapidjson::GenericObject<false, rapidjson::Value>& json,
        rapidjson::MemoryPoolAllocator<>& allocator)
    {
        char indexBuffer[MAX_INDEX_LENGTH];

        for (auto& location : locations)
        {
            //C++ style, something broken
            //std::stringstream ss;
            //ss << location.m_id;
            //const char* cindex = ss.str().c_str();
            
            //C style
#if WIN32
            sprintf_s(indexBuffer, "%i", location.m_id);
#else
            sprintf(indexBuffer, "%i", location.m_id);
#endif
            const char* cindex = indexBuffer;

            //NOTE: Casting integers to strings is not optimal
            rapidjson::Value vindex(rapidjson::kStringType);
            vindex.SetString(cindex, allocator);

            //TODO: Dont reallocate object memory every checkpoint!!!
            // This will incurr slowdown everycheckpoint
            json.EraseMember(vindex.Move());

            rapidjson::Value jsonLocation(rapidjson::kObjectType);
            {
                rapidjson::Value coordinates(rapidjson::kArrayType);
                coordinates.PushBack(location.m_xcoord, allocator);
                coordinates.PushBack(location.m_ycoord, allocator);
                jsonLocation.AddMember("coordinates", coordinates, allocator);

                rapidjson::Value neighbours(rapidjson::kArrayType);
                for (auto& neighbour : location.m_neighbours)
                {
                    neighbours.PushBack(neighbour, allocator);
                }
                jsonLocation.AddMember("neighbours", neighbours, allocator);

                rapidjson::Value hosts(rapidjson::kArrayType);
                for (auto& host : location.m_hosts)
                {
                    hosts.PushBack(host, allocator);
                }
                jsonLocation.AddMember("hosts", hosts, allocator);

                jsonLocation.AddMember("v-capacity", location.m_vectorState.Capacity, allocator);

                jsonLocation.AddMember("v-immature", location.m_vectorState.Immature, allocator);

                rapidjson::Value seir(rapidjson::kArrayType);
                seir.PushBack(location.m_vectorState.SEIR[0], allocator);
                seir.PushBack(location.m_vectorState.SEIR[1], allocator);
                seir.PushBack(location.m_vectorState.SEIR[2], allocator);
                seir.PushBack(location.m_vectorState.SEIR[3], allocator);
                jsonLocation.AddMember("v-state", seir, allocator);
            }

            json.AddMember(vindex.Move(), jsonLocation.Move(), allocator);
        }
    }

    void RapidJSONHelper::SaveHosts(
        const vector<Host>& hosts,
        const rapidjson::GenericObject<false, rapidjson::Value>& json,
        rapidjson::MemoryPoolAllocator<>& allocator)
    {
        char cindex[MAX_INDEX_LENGTH];

        for (auto& host : hosts)
        {
#if WIN32
            sprintf_s(cindex, "%i", host.m_hostId);
#else
            sprintf(cindex, "%i", host.m_hostId);
#endif
            rapidjson::Value vindex(rapidjson::kStringType);
            vindex.SetString(cindex, allocator);

            //TODO: Dont reallocate object memory every checkpoint!!!
            // This will incurr slowdown everycheckpoint
            json.EraseMember(vindex.Move());

            rapidjson::Value jsonHost(rapidjson::kObjectType);
            {
                jsonHost.AddMember("location", host.m_locationId, allocator);
                jsonHost.AddMember("i-state", InfectionStateToString(host.m_infectionState), allocator);
                jsonHost.AddMember("home", host.m_homeId, allocator);
                jsonHost.AddMember("hub", host.m_hubId, allocator);
            }
            json.AddMember(vindex.Move(), jsonHost.Move(), allocator);
        }
    }
}