#include "stdafx.h"
#include "epidemica/Types.h"
#include "epidemica/state/SimulationState.h" 
#include "epidemica/state/Host.h"
#include "epidemica/state/Location.h"

//optional implementations
#include "epidemica/scenario/PTreeSimulationScenario.h"
#include "epidemica/scenario/JSONSimulationScenario.h"

Epidemica::SimulationState::SimulationState()
{
}

Epidemica::SimulationState::SimulationState(const Epidemica::ISimulationScenario& scenario)
{
    scenario.LoadState(*this);
}

Epidemica::SimulationState::~SimulationState()
{
    
}

seir_vector Epidemica::SimulationState::GetHostsEpidemic() const
{
    seir_vector hostsEpidemic;
    for (auto host : m_hosts)
    {
        hostsEpidemic[host.m_infectionState]++;
    }
    return hostsEpidemic;
}

bool Epidemica::SimulationState::IsDayTime()
{
    return (m_config.CurrentCycle % 2) == 0;
}

std::ostream& Epidemica::operator<<(std::ostream & stm, const Epidemica::SimulationState& a)
{
    return stm << "State: Hosts = " << a.m_hosts.size() << " : Locations = " << a.m_locations.size();
}