
#include "epidemica/state/Config.h"
#include "epidemica/helper/RapidJSONHelper.h"

#include <string>



namespace Epidemica
{
    CheckpointMode ParseCheckpoint(const char* type)
    {
        if (!strcmp(type, "none")) return CheckpointMode::NONE;
        else if(!strcmp(type, "cycles")) return CheckpointMode::CYCLES;
        else if(!strcmp(type, "realtime")) return CheckpointMode::REALTIME;
        else return CheckpointMode::UNKNOWN;
    }

    Config::Config() { }

    Config::~Config() { }

#if RAPIDJSON_FOUND
    Config::Config(const rapidjson::GenericObject<true, rapidjson::Value>& config)
    {
        Load(config);
    }

    
    void Config::Load(const rapidjson::GenericObject<true, rapidjson::Value>& config)
    {
        //cycles
        CurrentCycle = RapidJSONHelper::ParseValueOrDefault<int>(config, "current-cycle", 0);

        //total-cycles
        if (config.HasMember("duration")) //support old name
        {
            TotalCycles = RapidJSONHelper::ParseValue<int>(config, "duration");
        }
        else
        {
            TotalCycles = RapidJSONHelper::ParseValue<int>(config, "total-cycles");
        }

        //Checkpoint
        if (config.HasMember("checkpoint") && config["checkpoint"].IsObject())
        {
            const auto& checkpoint = config["checkpoint"].GetObject();
            Mode = ParseCheckpoint(RapidJSONHelper::ParseValue<const char*>(checkpoint, "type"));
            CheckpointInterval = RapidJSONHelper::ParseValue<int>(checkpoint, "interval");
        }
    }

    void Config::Save(const rapidjson::GenericObject<false, rapidjson::Value>& config, rapidjson::MemoryPoolAllocator<>& allocator) const
    {
        //config["params-file"].Set<int>(TotalCycles);
        //config["hosts-file"].Set<int>(TotalCycles);
        //config["locations-file"].Set<int>(TotalCycles);
        //config["datalog-file"].Set<int>(TotalCycles);


        RapidJSONHelper::SafeSerializeValue<int>(config, "current-cycle", CurrentCycle, allocator);
        RapidJSONHelper::SafeSerializeValue<int>(config, "total-cycles", TotalCycles, allocator);
        
        if (Mode != CheckpointMode::NONE)
        {
            //TODO: guide here https://stackoverflow.com/questions/32896695/rapidjson-add-external-sub-document-to-document
            rapidjson::Value checkpoint(rapidjson::kObjectType);
            checkpoint.AddMember("type", "none", allocator);
            checkpoint.AddMember<int>("interval", 0, allocator);
            config.AddMember("checkpoint", checkpoint, allocator);
        }

        //seed
        if (config.HasMember("seed"))
        {
            config["seed"].Set<unsigned int>(Seed);
        }
        else
        {
            config.AddMember("seed", Seed, allocator);
        }
    }
#endif
}