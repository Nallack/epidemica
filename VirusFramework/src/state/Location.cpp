#include "stdafx.h"
#include <stdexcept>
#include "epidemica/state/Location.h"

Epidemica::Location::Location(
    int id,
    float xcoord, 
    float ycoord, 
    int vectorCapacity, 
    VectorState& vectorState, 
    neighbour_vector& neighbours,
    host_set& hosts) :
        m_id(id),
        m_xcoord(xcoord),
        m_ycoord(ycoord),
        m_vectorState(vectorState),
        m_neighbours(neighbours),
        m_hosts(hosts)
{
}

void* Epidemica::Location::ToBuffer()
{
    int maxNeighbours = (int)m_neighbours.size();
    int maxHosts = (int)m_neighbours.size();
    return nullptr;
}

#if BOOST_FOUND
Epidemica::Location::Location(int id, const boost::property_tree::ptree& location)
    : m_id(id)
{
    { //Position
        auto cit = location.get_child("coordinates").begin();
        m_xcoord = cit++->second.get_value<float>();
        m_ycoord = cit++->second.get_value<float>();
    }

    { //Vector State
        auto sit = location.get_child("v-state").begin();
        m_vectorState = VectorState();
        m_vectorState.Capacity = location.get<int>("v-capacity");
        m_vectorState.Immature = location.get<int>("v-immature");
        m_vectorState.SEIR[InfectionState::S] = sit++->second.get_value<int>();
        m_vectorState.SEIR[InfectionState::E] = sit++->second.get_value<int>();
        m_vectorState.SEIR[InfectionState::I] = sit++->second.get_value<int>();
        m_vectorState.SEIR[InfectionState::R] = sit++->second.get_value<int>();
    }
    { //neighbours
        auto node = location.get_child("neighbours");
		#if USE_VALARRAY
        m_neighbours = neighbour_vector(node.size());
		#else
		throw std::runtime_error("not implemented");
		#endif
        for (auto it = node.begin(); it != node.end(); it++)
        {
            int i = 0;
            m_neighbours[i++] = it->second.get_value<int>();
        }
    }
    { //hosts
        auto node = location.get_child("hosts");
        //m_hosts = host_set(node.size()); //std::set
        m_hosts = host_set(); //std::array
        for (auto it = node.begin(); it != node.end(); it++)
        {
            //int i = 0;
            //m_hosts[i++] = it->second.get_value<int>();
            m_hosts.insert(it->second.get_value<int>());
        }
    }
}
#endif

#if RAPIDJSON_FOUND
Epidemica::Location::Location(int id, const rapidjson::GenericObject<true, rapidjson::Value>& location)
    : m_id(id)
{
    { //Position
        auto cit = location["coordinates"].Begin();
        m_xcoord = cit++->GetFloat();
        m_ycoord = cit++->GetFloat();
    }

    { //Vector State
        m_vectorState = VectorState();
        m_vectorState.Capacity = location["v-capacity"].GetInt();
        m_vectorState.Immature = location["v-immature"].GetInt();
        auto sit = location["v-state"].Begin();
        m_vectorState.SEIR[InfectionState::S] = sit++->GetInt();
        m_vectorState.SEIR[InfectionState::E] = sit++->GetInt();
        m_vectorState.SEIR[InfectionState::I] = sit++->GetInt();
        m_vectorState.SEIR[InfectionState::R] = sit++->GetInt();
    }
    { //neighbours
        auto node = location["neighbours"].GetArray();
        m_neighbours = neighbour_vector(node.Capacity());
        int i = 0;
        for (auto it = node.Begin(); it != node.End(); it++)
        {
            m_neighbours[i++] = it->GetInt();
        }
    }
    { //hosts
        auto node = location["hosts"].GetArray();
        m_hosts = host_set();
        for (auto it = node.begin(); it != node.end(); it++)
        {
            m_hosts.insert(it->GetInt());
        }
    }
}
#endif

#if MPI_FOUND

MPI_Datatype Epidemica::Location::s_mpi_datatype = 0;

void Epidemica::Location::InitMPILayout()
{
    if (s_mpi_datatype != 0)
    {
        throw std::runtime_error("mpi host already initialized");
    }

    //May want to consider fetching MPI_Datatype for neighbours and hosts
    MPI_Datatype types[6] =
    {
        MPI_INT, //locationid
        MPI_FLOAT, //xcoord
        MPI_FLOAT, //ycoord
        //MPI_INT, //vectorcapacity
        MPI_INT, //vectorState
        MPI_INT, //neighbours
        MPI_INT, //hosts
    };

    int blocklengths[6] =
    {
        1,
        1,
        1,
        1,
        MAX_NEIGHBORS,
        MAX_HOSTS,
    };

    MPI_Aint offsets[6];
    offsets[0] = offsetof(struct Location, m_id);
    offsets[1] = offsetof(struct Location, m_xcoord);
    offsets[2] = offsetof(struct Location, m_ycoord);
    offsets[3] = offsetof(struct Location, m_vectorState);
    offsets[4] = offsetof(struct Location, m_neighbours);
    offsets[5] = offsetof(struct Location, m_hosts);

    MPI_Type_create_struct(6, blocklengths, offsets, types, &s_mpi_datatype);
    MPI_Type_commit(&s_mpi_datatype);
}
#endif