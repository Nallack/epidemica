#include "stdafx.h"

#include "epidemica/state/sequential/SequentialSimulationState.h"
#include "epidemica/scenario/PTreeSimulationScenario.h"

Epidemica::Sequential::SimulationState::SimulationState()
{
}

#if BOOST_FOUND
namespace pt = boost::property_tree;
Epidemica::Sequential::SimulationState::SimulationState(const ISimulationScenario& scenario)
{
     
}
#endif

Epidemica::Sequential::SimulationState::~SimulationState()
{
    
}

seir_vector Epidemica::Sequential::SimulationState::GetHostsEpidemic() const
{
    seir_vector hostsEpidemic;
    return hostsEpidemic;
}

bool Epidemica::Sequential::SimulationState::IsDayTime()
{
    return (CurrentCycle % 2) == 0;
}
