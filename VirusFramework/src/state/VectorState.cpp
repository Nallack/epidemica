#include "epidemica/state/VectorState.h"


Epidemica::InfectionState Epidemica::StringToInfectionState(const char* character)
{
    switch (character[0])
    {
    case 'S':
        return Epidemica::InfectionState::S;
        break;
    case 'E':
        return Epidemica::InfectionState::E;
        break;
    case 'I':
        return Epidemica::InfectionState::I;
        break;
    case 'R':
        return Epidemica::InfectionState::R;
        break;
    default:
        throw std::invalid_argument("invalid argument : " + std::string(character));
    }
}

std::string Epidemica::InfectionStateToString(InfectionState state)
{
    switch (state)
    {
    case InfectionState::S:
        return std::string("S");
        break;
    case InfectionState::E:
        return std::string("E");
        break;
    case InfectionState::I:
        return std::string("I");
        break;
    case InfectionState::R:
        return std::string("R");
        break;
    default:
        return std::string("Error!");
    }
}