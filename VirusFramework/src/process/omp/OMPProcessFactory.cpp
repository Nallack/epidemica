#if OPENMP_FOUND
#include "stdafx.h"
#include "epidemica/process/IProcess.h"
#include "epidemica/process/omp/OMPProcessFactory.h"

#include <epidemica/process/omp/OMPInfectionInHosts.h>
#include <epidemica/process/omp/OMPInfectionInVectors.h>
#include <epidemica/process/omp/OMPVectorMovement.h>
#include <epidemica/process/omp/OMPRegularHumanMovement.h>
#include <epidemica/process/omp/OMPVectorInfection.h>
#include <epidemica/process/omp/OMPHostInfection.h>
#include <epidemica/process/omp/OMPVectorPopulation.h>

using namespace Epidemica;

Epidemica::OpenMPProcessFactory::OpenMPProcessFactory()
{
}

Epidemica::OpenMPProcessFactory::~OpenMPProcessFactory()
{
}

IProcess* Epidemica::OpenMPProcessFactory::CreateProcess(std::string processName)
{
    throw "Not implemented";
}

IProcess* Epidemica::OpenMPProcessFactory::CreateInfectionInVectors()
{
    return new Epidemica::OMP::OMPInfectionInVectors();
}

IProcess* Epidemica::OpenMPProcessFactory::CreateInfectionInHosts()
{
    return new Epidemica::OMP::OMPInfectionInHosts();
}

IProcess* Epidemica::OpenMPProcessFactory::CreateRegularHumanMovement()
{
    return new Epidemica::OMP::OMPRegularHumanMovement();
}

IProcess* Epidemica::OpenMPProcessFactory::CreateVectorMovement()
{
    return new Epidemica::OMP::OMPVectorMovement();
}

IProcess* Epidemica::OpenMPProcessFactory::CreateVectorInfection()
{
    return new Epidemica::OMP::OMPVectorInfection();
}

IProcess* Epidemica::OpenMPProcessFactory::CreateHostInfection()
{
    return new Epidemica::OMP::OMPHostInfection();
}

IProcess* Epidemica::OpenMPProcessFactory::CreateVectorPopulation()
{
    return new Epidemica::OMP::OMPVectorPopulation();
}
#endif