#ifdef OPENMP_FOUND
#include "stdafx.h"
#include "epidemica/process/omp/OMPInfectionInHosts.h"
#include "epidemica/state/SimulationState.h"

#include <cmath>
#include <omp.h>

Epidemica::OMP::OMPInfectionInHosts::OMPInfectionInHosts()
{
    m_generator = std::default_random_engine();
}

Epidemica::OMP::OMPInfectionInHosts::~OMPInfectionInHosts()
{
}

std::string Epidemica::OMP::OMPInfectionInHosts::GetName() const
{
    return "OMP::OMPInfectionInHosts";
}

void Epidemica::OMP::OMPInfectionInHosts::Execute(SimulationState& state)
{
    /*
    Original Python
    eToIRate = params['host-e-to-i-rate'] # see[1]
    iToRRate = params['host-i-to-r-rate'] # see[1]
    pITrans = 1.0 - math.exp(-eToIRate)
    pRTrans = 1.0 - math.exp(-iToRRate)
    for iid in hosts :
        h = hosts[iid]
        s = h['i-state']
        if s == 'E' :
            if binSamp(1, pITrans) : h['i-state'] = 'I'
        elif s == 'I' :
            if binSamp(1, pRTrans) : h['i-state'] = 'R'
    */

    double pITrans = 1.0f - std::exp(-state.m_params.hostEIRate);
    double pRTrans = 1.0f - std::exp(-state.m_params.hostIRRate);


    #pragma omp parallel for
    for (int i = 0; i < (int)state.m_hosts.size(); i++)
    {
        if (state.m_hosts[i].m_infectionState == InfectionState::E)
        {
            std::binomial_distribution<int> binomial(1, pITrans);
            //TODO: shouldnt need a binomial sample here, bournouli trial should be fine
            int sample = binomial(m_generator);
            if(sample) state.m_hosts[i].m_infectionState = InfectionState::I;
        }

        else if (state.m_hosts[i].m_infectionState == InfectionState::I)
        {
            std::binomial_distribution<int> binomial(1, pRTrans);
            //TODO: shouldnt need a binomial sample, bournouli trial should be fine
            int sample = binomial(m_generator);
            if(sample) state.m_hosts[i].m_infectionState = InfectionState::R;
        }
    }

}
#endif