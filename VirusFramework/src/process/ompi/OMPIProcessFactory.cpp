#include "stdafx.h"
#if MPI_FOUND & OPENMP_FOUND
#include "epidemica/process/ompi/OMPIProcessFactory.h"

#include "epidemica/process/st/STProcesses.h"

#include <stdexcept>
#include <exception>

using namespace Epidemica;

Epidemica::OMPIProcessFactory::OMPIProcessFactory()
{
}

Epidemica::OMPIProcessFactory::~OMPIProcessFactory()
{
}

void Epidemica::OMPIProcessFactory::Initialize()
{
	int argc = 0;
	char** argv = {};

	//TODO: potentially do OMPI init here
	//MPI_Init(&argc, &argv);
	//MPI_Get()
	//std::cout << ""
}

//TODO: may want to introduce a fallback system instead
ProcessPipeline* Epidemica::OMPIProcessFactory::CreateProcessPipeline()
{
	//TODO: make sure something is disposing these processes!
	std::vector<std::shared_ptr<IProcess>> processes = { std::shared_ptr<IProcess>(new ST::STVectorPopulation()) };
	return new ProcessPipeline(processes);
}

IProcess* Epidemica::OMPIProcessFactory::CreateProcess(std::string processName)
{
	throw std::runtime_error("Not implemented");
}

IProcess* Epidemica::OMPIProcessFactory::CreateInfectionInVectors()
{
	throw std::runtime_error("Not implemented");
}

IProcess* Epidemica::OMPIProcessFactory::CreateInfectionInHosts()
{
	throw std::runtime_error("Not implemented");
}

IProcess* Epidemica::OMPIProcessFactory::CreateRegularHumanMovement()
{
	throw std::runtime_error("Not implemented");
}

IProcess* Epidemica::OMPIProcessFactory::CreateVectorMovement()
{
	throw std::runtime_error("Not implemented");
}

IProcess* Epidemica::OMPIProcessFactory::CreateVectorInfection()
{
	throw std::runtime_error("Not implemented");
}

IProcess* Epidemica::OMPIProcessFactory::CreateHostInfection()
{
	throw std::runtime_error("Not implemented");
}

IProcess* Epidemica::OMPIProcessFactory::CreateVectorPopulation()
{
	throw std::runtime_error("Not implemented");
}

void Epidemica::OMPIProcessFactory::Finalize()
{
	//MPI_Finalize();
}
#endif