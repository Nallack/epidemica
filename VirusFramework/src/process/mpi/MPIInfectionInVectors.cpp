#include "stdafx.h"

#include "epidemica/process/mpi/MPIInfectionInVectors.h"
#include "epidemica/state/SimulationState.h"
#include "epidemica/state/Location.h"
#include "epidemica/state/Params.h"

#include <cmath>
#include <random>

Epidemica::MPI::MPIInfectionInVectors::MPIInfectionInVectors()
{
    m_generator = random_engine();
}

Epidemica::MPI::MPIInfectionInVectors::~MPIInfectionInVectors()
{
}

std::string Epidemica::MPI::MPIInfectionInVectors::GetName() const
{
    return "MPI::MPIInfectionInVectors";
}

void Epidemica::MPI::MPIInfectionInVectors::Execute(SimulationState& state)
{
    throw new std::runtime_error("not implemented");
    //double pTrans = 1.0 - std::exp(-state.m_params.vectorEIRate);
    //for(unsigned int i = 0; i < state.m_locations.size(); i++)
    //{
    //    int ecount = state.m_locations[i].m_vectorState.SEIR[InfectionState::E];
    //    if (ecount == 0) continue;
    //    std::binomial_distribution<int> binomial(ecount, pTrans);
    //    int sample = binomial(m_generator);
    //    state.m_locations[i].m_vectorState.SEIR[InfectionState::E] -= sample;
    //    state.m_locations[i].m_vectorState.SEIR[InfectionState::I] += sample;
    //}
}
