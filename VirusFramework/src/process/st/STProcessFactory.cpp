#include "stdafx.h"
#include "epidemica/process/IProcess.h"
#include "epidemica/process/st/STProcessFactory.h"
#include "epidemica/process/st/STInfectionInVectors.h"
#include "epidemica/process/st/STInfectionInHosts.h"
#include "epidemica/process/st/STRegularHumanMovement.h"
#include "epidemica/process/st/STVectorMovement.h"
#include "epidemica/process/st/STVectorInfection.h"
#include "epidemica/process/st/STHostInfection.h"
#include "epidemica/process/st/STVectorPopulation.h"

using namespace Epidemica;

Epidemica::STProcessFactory::STProcessFactory()
{
}

Epidemica::STProcessFactory::~STProcessFactory()
{
}

IProcess* Epidemica::STProcessFactory::CreateProcess(std::string processName)
{
    throw "Not implemented";
}

IProcess* Epidemica::STProcessFactory::CreateInfectionInVectors()
{
    return new Epidemica::ST::STInfectionInVectors();
}

IProcess* Epidemica::STProcessFactory::CreateInfectionInHosts()
{
    return new Epidemica::ST::STInfectionInHosts();
}

IProcess* Epidemica::STProcessFactory::CreateRegularHumanMovement()
{
    return new Epidemica::ST::STRegularHumanMovement();
}

IProcess* Epidemica::STProcessFactory::CreateVectorMovement()
{
    return new Epidemica::ST::STVectorMovement();
}

IProcess* Epidemica::STProcessFactory::CreateVectorInfection()
{
    return new Epidemica::ST::STVectorInfection();
}

IProcess* Epidemica::STProcessFactory::CreateHostInfection()
{
    return new Epidemica::ST::STHostInfection();
}

IProcess* Epidemica::STProcessFactory::CreateVectorPopulation()
{
    return new Epidemica::ST::STVectorPopulation();
}
