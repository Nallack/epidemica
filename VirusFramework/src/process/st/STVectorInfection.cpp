#include "stdafx.h"
#include "epidemica/process/st/STVectorInfection.h"

#include "epidemica/state/SimulationState.h"
#include "epidemica/state/Location.h"
#include "epidemica/state/Params.h"

#include <easylogging++.h>

Epidemica::ST::STVectorInfection::STVectorInfection()
{
    m_generator = std::default_random_engine();
}

Epidemica::ST::STVectorInfection::~STVectorInfection()
{
}

std::string Epidemica::ST::STVectorInfection::GetName() const
{
    return "ST::STVectorInfection";
}

void Epidemica::ST::STVectorInfection::Execute(SimulationState& state)
{
    RealType bitingRate = state.m_params.bitingRate;
    RealType infectionProbability = state.m_params.vectorInfectionProbability;

    for (int locationId = 0; locationId < state.m_locations.size(); locationId++)
    {
        Location& location = state.m_locations[locationId];
        int susceptible = location.m_vectorState.SEIR[InfectionState::S];

        if (susceptible == 0) continue;

        int hosts = 0;
        int infectedHosts = 0;

        //consider doing this aggregation elsewhere
        for(auto& hostId : location.m_hosts)
        {
            hosts += 1;
            if (state.m_hosts[hostId].m_infectionState == InfectionState::I)
            {
                infectedHosts += 1;
            }
        }

        if (infectedHosts == 0) continue;

        //rate is proportional to the number hosts already infected
        RealType beta = (RealType)(infectedHosts / hosts) * bitingRate * infectionProbability;
        RealType p = (RealType)1.0 - std::exp(-beta);

        std::binomial_distribution<int> binomial(susceptible, p);
        int newInfections = binomial(m_generator);

        location.m_vectorState.SEIR[InfectionState::S] -= newInfections;
        location.m_vectorState.SEIR[InfectionState::E] += newInfections;
    }
}
