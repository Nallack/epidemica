#include "stdafx.h"
#include "epidemica/process/st/STVectorPopulation.h"

#include "epidemica/state/SimulationState.h"
#include "epidemica/state/Location.h"
#include "epidemica/state/Params.h"

#include <valarray>

#include <easylogging++.h>

Epidemica::ST::STVectorPopulation::STVectorPopulation()
{
    m_generator = random_engine();
}

Epidemica::ST::STVectorPopulation::~STVectorPopulation()
{
}

std::string Epidemica::ST::STVectorPopulation::GetName() const
{
    return "ST::STVectorPopulation";
}

void Epidemica::ST::STVectorPopulation::Execute(SimulationState& state)
{

    double birthRate = state.m_params.vectorBirthRate;
    double deathProbability = 1.0 - std::exp(-state.m_params.vectorDeathRate);
    double matureProbability = 1.0 - std::exp(-state.m_params.vectorMaturationRate);

    for (Location& location : state.m_locations)
    {
        std::valarray<int> deaths = std::valarray<int>(INFECTION_STATES);
        int vectorPopulation = 0;
        for (int i = 0; i < INFECTION_STATES; i++)
        {
            //TODO: can we precalculate this?
            vectorPopulation += location.m_vectorState.SEIR[i];

            std::binomial_distribution<int> binomial(location.m_vectorState.SEIR[i], deathProbability);
            deaths[i] = binomial(m_generator);
        }

        double birthProbability = (float)vectorPopulation * birthRate * (1.0 - (vectorPopulation / location.m_vectorState.Capacity));

        int births = 0;
        if (birthProbability > 0.0)
        {
            std::poisson_distribution<int> poisson(birthProbability);
            births = poisson(m_generator);
        }

        std::binomial_distribution<int> binomial(location.m_vectorState.Immature, matureProbability);
        int matured = binomial(m_generator);

        //TODO vector subtraction
        //location.m_vectorState.SEIR -= deaths;
        for (int i = 0; i < INFECTION_STATES; i++)
        {
            location.m_vectorState.SEIR[i] -= deaths[i];
        }

        location.m_vectorState.Immature += births;
        location.m_vectorState.Immature -= matured;
        location.m_vectorState.SEIR[InfectionState::S] += matured;
    }

}