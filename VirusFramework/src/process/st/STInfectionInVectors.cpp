#include "stdafx.h"

#include "epidemica/process/st/STInfectionInVectors.h"
#include "epidemica/state/SimulationState.h"
#include "epidemica/state/Location.h"
#include "epidemica/state/Params.h"

#include <cmath>
#include <random>

Epidemica::ST::STInfectionInVectors::STInfectionInVectors()
{
    m_generator = random_engine();
}

Epidemica::ST::STInfectionInVectors::~STInfectionInVectors()
{
}

std::string Epidemica::ST::STInfectionInVectors::GetName() const
{
    return "ST::STInfectionInVectors";
}

void Epidemica::ST::STInfectionInVectors::Execute(SimulationState& state)
{
    double pTrans = 1.0 - std::exp(-state.m_params.vectorEIRate);
    for(unsigned int i = 0; i < state.m_locations.size(); i++)
    {
        int ecount = state.m_locations[i].m_vectorState.SEIR[InfectionState::E];
        if (ecount == 0) continue;
        std::binomial_distribution<int> binomial(ecount, pTrans);
        int sample = binomial(m_generator);
        state.m_locations[i].m_vectorState.SEIR[InfectionState::E] -= sample;
        state.m_locations[i].m_vectorState.SEIR[InfectionState::I] += sample;
    }
}
