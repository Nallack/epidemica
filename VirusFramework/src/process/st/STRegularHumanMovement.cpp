#include "stdafx.h"
#include "epidemica/process/st/STRegularHumanMovement.h"

#include "epidemica/state/SimulationState.h"
#include "epidemica/state/Location.h"
#include "epidemica/state/Params.h"

Epidemica::ST::STRegularHumanMovement::STRegularHumanMovement()
{
}

Epidemica::ST::STRegularHumanMovement::~STRegularHumanMovement()
{
}

std::string Epidemica::ST::STRegularHumanMovement::GetName() const
{
    return "ST::STRegularHumanMovement";
}

void Epidemica::ST::STRegularHumanMovement::Execute(SimulationState& state)
{
    for (int i = 0; i < state.m_hosts.size(); i++)
    {
        Host& host = state.m_hosts[i];

        int src = host.m_locationId;
        int dst = state.IsDayTime() ? host.m_hubId : host.m_homeId;
        if (host.m_hubId < 0) dst = host.m_homeId; //Can we do this without if?
        host.m_locationId = dst;
        state.m_locations[src].m_hosts.erase(host.m_hostId);
        state.m_locations[dst].m_hosts.insert(host.m_hostId);
    }
}