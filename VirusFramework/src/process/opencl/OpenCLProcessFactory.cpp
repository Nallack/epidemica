#ifdef OPENCL_FOUND

#include "stdafx.h"
#include "epidemica/process/opencl/OpenCLProcessFactory.h"
#include "epidemica/process/IProcess.h"

using namespace Epidemica;

Epidemica::OpenCLProcessFactory::OpenCLProcessFactory()
{
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);
    m_platform = platforms[0];
    
    std::vector<cl::Device> devices;
    m_platform.getDevices(CL_DEVICE_TYPE_GPU | CL_DEVICE_TYPE_CPU, &devices);
    m_device = devices[0];
    
    m_context = cl::Context(m_device);

    //std::cout << "loaded platform : " << m_platform.getInfo<CL_PLATFORM_NAME>() << std::endl;
    //std::cout << "loaded device : " << m_device.getInfo<CL_DEVICE_NAME>() << std::endl;
}

Epidemica::OpenCLProcessFactory::~OpenCLProcessFactory()
{
}

IProcess* Epidemica::OpenCLProcessFactory::CreateProcess(std::string processName)
{
    throw "Not implemented";
}

IProcess* Epidemica::OpenCLProcessFactory::CreateInfectionInVectors()
{
    throw "Not implemented";
}

IProcess* Epidemica::OpenCLProcessFactory::CreateInfectionInHosts()
{
    throw "Not implemented";
}

IProcess* Epidemica::OpenCLProcessFactory::CreateRegularHumanMovement()
{
    throw "Not implemented";
}

IProcess* Epidemica::OpenCLProcessFactory::CreateVectorMovement()
{
    throw "Not implemented";
}

IProcess* Epidemica::OpenCLProcessFactory::CreateVectorInfection()
{
    throw "Not implemented";
}

IProcess* Epidemica::OpenCLProcessFactory::CreateHostInfection()
{
    throw "Not implemented";
}

IProcess* Epidemica::OpenCLProcessFactory::CreateVectorPopulation()
{
    throw "Not implemented";
}


#endif