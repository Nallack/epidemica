#include "stdafx.h"
#include "epidemica/process/AbstractProcessFactory.h"
#include "epidemica/process/IProcess.h"
#include "epidemica/process/ProcessPipeline.h"
#include "epidemica/state/SimulationState.h"
#include "epidemica/profiling/TimeProfiler.h"
#include "epidemica/profiling/Stopwatch.h"

#include <easylogging++.h>

Epidemica::ProcessPipeline::ProcessPipeline()
{

}

Epidemica::ProcessPipeline::ProcessPipeline(const std::vector<std::shared_ptr<IProcess>>& processes) :
    m_processes(processes)
{
}

Epidemica::ProcessPipeline::~ProcessPipeline()
{
}

void Epidemica::ProcessPipeline::Execute(Epidemica::SimulationState& state, Epidemica::TimeProfiler* profiler)
{
    if(profiler == nullptr)
    {
        for (auto it = m_processes.begin(); it != m_processes.end(); it++)
        {
            (*it)->Execute(state);
        }
    }
    else
    {
        Epidemica::Stopwatch stopwatch;
        for (auto it = m_processes.begin(); it != m_processes.end(); it++)
        {
            stopwatch.Restart();
            (*it)->Execute(state);

            stopwatch.Stop();
            double time = stopwatch.GetTime<double>();
            profiler->AddTime((*it)->GetName(), time);
        }
    }

    //TODO, this increment is a little out of place, but its not worth
    //creating a seperate process for it.
    state.m_config.CurrentCycle += 1;
    LOG(TRACE) << "cycle : " << state.m_config.CurrentCycle;

}

std::ostream& Epidemica::operator<<(std::ostream& os, const Epidemica::ProcessPipeline& a)
{
    os << "ProcessPipeline:";
    for (auto it = a.m_processes.cbegin(); it != a.m_processes.cend(); it++)
    {
        os << std::endl << **it;
    }
    return os;
}
