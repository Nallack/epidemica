
# The idea for this function came from here:
# http://stackoverflow.com/questions/31422680/how-to-set-visual-studio-filters-for-nested-sub-directory-using-cmake
# basically, it will put filters in various IDEs based on the source code paths



function(auto_source_group)
	foreach(_source IN ITEMS ${ARGN})
		if (IS_ABSOLUTE "${_source}")
			file(RELATIVE_PATH _source_rel "${CMAKE_CURRENT_SOURCE_DIR}" "${_source}")
		else()
			set(_source_rel "${_source}")
		endif()
		get_filename_component(_source_path "${_source_rel}" PATH)
		#TODO: may need to be changed for other IDEs
		string(REPLACE "/" "\\" source_path_msvc "${_source_path}")
		source_group("${source_path_msvc}" FILES "${_source_rel}")
	endforeach()
endfunction(auto_source_group)

function(auto_filter_group FILTER_NAME)
	foreach(_source IN ITEMS ${ARGN})
		if (IS_ABSOLUTE "${_source}")
			file(RELATIVE_PATH _source_rel "${CMAKE_CURRENT_SOURCE_DIR}" "${_source}")
		else()
			set(_source_rel "${_source}")
		endif()
		get_filename_component(_source_path "${_source_rel}" PATH)

		if(MSVC)
			string(REPLACE "/" "\\" source_path_msvc "${_source_path}")
		endif()
		source_group("${FILTER_NAME}\\${source_path_msvc}" FILES "${_source_rel}")
	endforeach()
endfunction(auto_filter_group)

function(auto_rel_filter_group FILTER_NAME SOURCE_DIR)
	foreach(_source IN ITEMS ${ARGN})
		if (IS_ABSOLUTE "${_source}")
			file(RELATIVE_PATH _source_rel "${CMAKE_CURRENT_SOURCE_DIR}/${SOURCE_DIR}" "${_source}")
		else()
			set(_source_rel "${_source}")
		endif()

		get_filename_component(_source_path "${_source}" PATH)
		if(NOT ${_source_path} STREQUAL "")
			string(REGEX REPLACE "${SOURCE_DIR}" "" _sub_dir ${_source_path})
		endif()
		set(_filter_path "${FILTER_NAME}/${_sub_dir}")

		if(MSVC)
			string(REPLACE "/" "\\" _filter_path "${_filter_path}")
		endif()
		source_group("${_filter_path}" FILES "${_source_rel}")
	endforeach()
endfunction(auto_rel_filter_group)