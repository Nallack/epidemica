#!/bin/bash

# define some color for later use
RED='\033[0;31m'
CYAN='\033[0;36m'
NC='\033[0m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
BROWN='\033[0;33m'


echo "---------------------"
echo "Epidemica build - for Magnus Supercomputer"
echo ""
echo "This script helps you build project Epidemica on Magnus."
echo ""
echo ""


printf "${BROWN}Loading modules for compiling..."
module swap PrgEnv-cray PrgEnv-gnu
module swap gcc gcc/4.9.2
module load boost/1.57.0
module load cmake/3.4.1
printf "${GREEN}Done.\n${NC}"

#Update third party libraries
git submodule update --init --recursive

# Checkout if dependences exist.
printf "${BROWN}Searching GCC:${NC}"
command -v gcc >/dev/null 2>&1 || { echo -e >&2 "${RED}ERROR: GCC not found.${NC}"; echo >&2 "Please make sure gcc is installed and path to GCC is linked onto \$PATH."; exit 1; }
# this command only execute when command above success.
command -v gcc

printf "${BROWN}Searching CMake:${NC}"
command -v cmake >/dev/null 2>&1 || { echo >&2 "${RED}ERROR: CMake not found.${NC}"; echo >&2 "Please make sure CMake is installed and path to CMake is linked onto \$PATH"; exit 1; }
# same, execute only when command above success
command -v cmake


printf "${BROWN}Searching Python 3 Module numpy.."
python -c "import numpy" >/dev/null 2>&1
if [ $? -ne 0 ]; then
	printf "${RED}FAILED!${NC}\n" >&2
	echo >&2 -e "Please make sure numpy is installed onto your system before running this script."
	exit 1
else
	printf "${GREEN}Success.\n${NC}"
fi


# When 2 basic application found, execute.
if [ ! -d builds ]; then
	printf "${BROWN}making folder \"builds\"....${NC}"
	mkdir builds
	printf "${GREEN}Done.\n${NC}"
	printf "${BROWN}Entering folder builds..\n${NC}"
	cd builds
	printf "${CYAN}The folder for storing all builds file will located at:\n${NC}"
	pwd
else
	printf "${BROWN}builds folder already exists, entering....\n${NC}"
	cd builds
	printf "${CYAN}The folder for storing all builds file will located at:\n${NC}"
	pwd
fi
echo ""

# folder created, let cmake do the jobs.
echo -e "${BROWN}Running CMake....${NC}"
cmake ../
if [ $? -ne 0 ]; then
	echo -e >&2 "${RED}------ERROR------${NC}"
	echo >&2 "seems CMake itself cannot finish. Please check for error shown above."
	exit 1
fi
echo -e "${GREEN}CMake seems completed.${NC}"
echo -e "${BROWN}Making the program.....${NC}"
make -j8
if [ $? -ne 0 ]; then
	echo -e >&2 "${RED}------ERROR------${NC}"
	echo >&2 "Seems make progress has some error, please check the error and send them to developer team."
	exit 1
fi

