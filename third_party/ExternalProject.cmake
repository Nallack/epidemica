
cmake_minimum_required(VERSION 2.6)

# This file contains a list of third party projects and builds
# using the External Project functionality of cmake

# NOTE: This method is COMPLETELY seperate to the third_party/ folder way
# and all third party libraries SHOULD be moved to this style UNLESS it
# is reasonable to have the library installed via a package manager or
# an installer

INCLUDE(ExternalProject)

#==================
# GTEST
#==================

#message("${CMAKE_CURRENT_LIST_DIR}/googletest")
#ExternalProject_Add(
#	googletest
#	SOURCE_DIR		"${PROJECT_BINARY_DIR}/googletest-src"
#	BINARY_DIR		"${PROJECT_BINARY_DIR}/bin")
#
#ExternalProject_Add(gtest)
#ExternalProject_Get_Property(gtest binary_dir)
#ExternalProject_Get_Property(gtest source_dir)