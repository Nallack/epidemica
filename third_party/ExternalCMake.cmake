
cmake_minimum_required(VERSION 2.6)

# this third_party approach relies on the git submodule feature.
# It will run and build a third_party cmake project IF it has been setup properly.
# a better approach is to use the ExternalProject command which can automatically
# download/pull and build any c++ project

# This approach does have the following advantage:
# * allows modifying a third party library
# * view the library source code in an IDE

#=========================
# GoogleTest
#==========================
#googletest contains gmock, gmock_main, gtest and gtest_main
#set(BUILD_SHARED_LIBS ON)
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
add_subdirectory(googletest)

set_property(TARGET gmock PROPERTY FOLDER "third_party")
set_property(TARGET gmock_main PROPERTY FOLDER "third_party")
set_property(TARGET gtest PROPERTY FOLDER "third_party")
set_property(TARGET gtest_main PROPERTY FOLDER "third_party")

set_target_properties(
	gmock PROPERTIES
	RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)
set_target_properties(
	gmock_main PROPERTIES
	RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)
set_target_properties(
	gtest PROPERTIES
	RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)
set_target_properties(
	gtest_main PROPERTIES
	RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)


enable_testing()
include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})


#======================
# EasyLogging++
#======================
#add_subdirectory(easyloggingpp)
#set_property(TARGET easyloggingpp PROPERTY FOLDER "third_party")
