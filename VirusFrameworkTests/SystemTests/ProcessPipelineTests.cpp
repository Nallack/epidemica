
#include <epidemica/scenario/JSONSimulationScenario.h>
#include <epidemica/scenario/ScenarioFactory.h>
#include <epidemica/Simulation.h>
#include <epidemica/DeviceManager.h>
#include <epidemica/process/ProcessPipeline.h>
#include <epidemica/state/SimulationState.h>
#include <memory>

#include <gtest/gtest.h>

namespace Epidemica
{
    TEST(DeviceManagerTest, STPipeline)
    {
        std::shared_ptr<DeviceManager> deviceManager = std::make_shared<DeviceManager>();
        auto pipeline = deviceManager->CreateArbovirusProcessPipeline("ST");
    }

    TEST(DeviceManagerTest, OMPPipeline)
    {
        std::shared_ptr<DeviceManager> deviceManager = std::make_shared<DeviceManager>();
        auto pipeline = deviceManager->CreateArbovirusProcessPipeline("OMP");
    }
}