
#include <epidemica/scenario/JSONSimulationScenario.h>
#include <epidemica/scenario/ScenarioFactory.h>
#include <epidemica/Simulation.h>
#include <epidemica/DeviceManager.h>
#include <epidemica/state/SimulationState.h>
#include <epidemica/process/ProcessPipeline.h>
#include <memory>

#include <gtest/gtest.h>

namespace Epidemica
{
    /**
     * @class	JSONScenarioTests
     *
     * @brief	Test case: test if the JSON file parsing is parsing correct scenario information.
     */

    class JSONScenarioTests : public testing::Test
    {
        std::shared_ptr<DeviceManager> m_deviceManager;
        std::shared_ptr<ProcessPipeline> m_emptyPipeline;

    public:
        virtual void SetUp() override
        {
            //Code here will be called immediately after the constructor (right before each test).
            m_deviceManager.reset(new DeviceManager());
            m_emptyPipeline.reset(new ProcessPipeline());
        }

        /**
         * Tests that the scenario can be loaded correctly.
         * @param filename Filename of the scenario to load.
         */
        void TestScenarioLoading(const std::string& filename)
        {
            std::unique_ptr<ISimulationScenario> scenario =
                std::unique_ptr<ISimulationScenario>(
                    ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));
        }

        /**
         * Tests scenario saving by saving a scenario to a file, loading it again and comparing.
         * @param filename Filename of the file.
         */
        void TestScenarioSaving(const std::string filename)
        {
            std::unique_ptr<ISimulationScenario> scenario =
                std::unique_ptr<ISimulationScenario>(
                    ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));

            scenario->SaveScenarioToFile("testsave", "");

            std::unique_ptr<ISimulationScenario> savedScenario =
                std::unique_ptr<ISimulationScenario>(
                    ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));

            JSONSimulationScenario& jsonScenario = static_cast<JSONSimulationScenario&>(*scenario);
            JSONSimulationScenario& jsonSavedScenario = static_cast<JSONSimulationScenario&>(*savedScenario);

            ASSERT_EQ(jsonScenario.GetParams(), jsonSavedScenario.GetParams());
            ASSERT_EQ(jsonScenario.GetHosts(), jsonSavedScenario.GetHosts());
            ASSERT_EQ(jsonScenario.GetLocations(), jsonSavedScenario.GetLocations());
            ASSERT_TRUE(jsonScenario.Compare(jsonSavedScenario));
        }

        /**
        * Executes the loading and saving test operation for JSON scenarios
        * @param filename Filename of the file.
        */
        void RunLoadingAndSavingJSONTest(const std::string& filename)
        {
            //Load a scenario fully, then save it back to a file.
            std::shared_ptr<ISimulationScenario> scenario =
                std::shared_ptr<ISimulationScenario>(
                    ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));

            std::shared_ptr<Simulation> simulation = std::shared_ptr<Simulation>(
                new Simulation("ST", m_deviceManager, scenario, m_emptyPipeline));

            simulation->Initialize();
            simulation->LoadStateFromScenario();
            simulation->SaveStateToScenario();
            simulation->Finalize();

            scenario->SaveScenarioToFile("testsave", "");

            // load the original scenario and saved scenario, there are now 3 different scenarios
            std::unique_ptr<ISimulationScenario> originalScenario =
                std::unique_ptr<ISimulationScenario>(
                    ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));

            std::unique_ptr<ISimulationScenario> savedScenario =
                std::unique_ptr<ISimulationScenario>(
                    ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(filename));

            JSONSimulationScenario& jsonOriginalScenario = static_cast<JSONSimulationScenario&>(*originalScenario);
            JSONSimulationScenario& jsonSavedScenario = static_cast<JSONSimulationScenario&>(*savedScenario);
            JSONSimulationScenario& jsonScenario = static_cast<JSONSimulationScenario&>(*scenario);

            //Compare the original with the final
            ASSERT_TRUE(jsonOriginalScenario.GetParams() == jsonScenario.GetParams());
            ASSERT_EQ(jsonOriginalScenario.GetHosts(), jsonScenario.GetHosts());
            ASSERT_EQ(jsonOriginalScenario.GetLocations(), jsonScenario.GetLocations());
            ASSERT_TRUE(jsonOriginalScenario.Compare(jsonScenario));


            //Compare the backup with the final
            ASSERT_EQ(jsonSavedScenario.GetParams(), jsonScenario.GetParams());
            ASSERT_EQ(jsonSavedScenario.GetHosts(), jsonScenario.GetHosts());
            ASSERT_EQ(jsonSavedScenario.GetLocations(), jsonScenario.GetLocations());
            ASSERT_TRUE(jsonSavedScenario.Compare(jsonScenario));

            //consider removing the testsave file from the bin dir here,
            // use rm on unix, something else on windows, or boost.
        }

    };

    // TEST FIXTURES

    TEST_F(JSONScenarioTests, test2x2)              { TestScenarioLoading("scenarios/test2x2/config.json"); }
    TEST_F(JSONScenarioTests, test2x2_legacy)       { TestScenarioLoading("scenarios/test2x2legacy/params.json"); }
    TEST_F(JSONScenarioTests, test5x4)              { TestScenarioLoading("scenarios/test5x4/test.config.json"); }
    TEST_F(JSONScenarioTests, test5x4_legacy)       { TestScenarioLoading("scenarios/test5x4legacy/test.params.json");}
    TEST_F(JSONScenarioTests, testDefaultConfig)    { TestScenarioLoading("taskconfig.json"); }
    TEST_F(JSONScenarioTests, test2x2Save)          { TestScenarioSaving("scenarios/test2x2/config.json");}
    TEST_F(JSONScenarioTests, test2x2LoadingAndSaving) { RunLoadingAndSavingJSONTest("scenarios/test2x2/config.json"); }
    TEST_F(JSONScenarioTests, test5x4LoadingAndSaving) { RunLoadingAndSavingJSONTest("scenarios/test5x4/test.config.json"); }

}