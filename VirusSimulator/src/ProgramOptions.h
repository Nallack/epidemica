#pragma once

#define PROGRAM_OPTIONS_BOOST 1
#define PROGRAM_OPTIONS_CXXOPTS 2

#if CXXOPTS_FOUND
#define PROGRAM_OPTIONS PROGRAM_OPTIONS_CXXOPTS
#elif BOOST_FOUND
#define PROGRAM_OPTIONS PROGRAM_OPTIONS_BOOST
#else
"Error: Program Options Library Missing"
#endif

#include <memory>

#if PROGRAM_OPTIONS == PROGRAM_OPTIONS_CXXOPTS
#include <cxxopts.hpp>
using program_options = cxxopts::Options;
void ParseArguments(int argc, char** argv, cxxopts::Options& options);
#endif

#if PROGRAM_OPTIONS == PROGRAM_OPTIONS_BOOST
#include <boost/program_options.hpp>
using program_options = boost::program_options::variables_map;
void ParseArguments(
    int argc,
    char** argv,
    boost::program_options::options_description& desc, 
    boost::program_options::variables_map& vm);
#endif