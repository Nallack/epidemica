#pragma once

#include <memory>
#include "ProgramOptions.h"

namespace Epidemica
{
    class Simulation;
    class DeviceManager;
    class PTreeSimulationScenario;
}

/**
* Performs the simulation loading steps with output.
* @param options       Options for controlling the operation.
* @param deviceManager Manager for device.
* @param scenario      The scenario.
* @return Null if it fails, else the simulation.
*/
Epidemica::Simulation* LoadSimulation(
    const program_options& options,
    std::shared_ptr<Epidemica::DeviceManager> deviceManager,
    std::shared_ptr<Epidemica::ISimulationScenario> scenario);


/**
* Executes the simulation and outputs the running time.
* @param [in,out] simulation The simulation.
*/
void RunSimulation(Epidemica::Simulation& simulation);

/**
* Process the results in an easily viewable format here. Current supports
* printstats and epicurve.
* @param [in,out] options    Options for controlling the operation.
* @param [in,out] simulation The simulation.
*/
void ProcessResults(program_options& vm, Epidemica::Simulation& simulation);
