#pragma once

#include <epidemica/Types.h>

namespace Epidemica
{
    class TimeProfiler;

#ifdef MATPLOTLIBCPP_PYTHON_HEADER
    /**
     * Displays an epidemic curve described by hostsEpidemic
     * @param hostsEpidemic The hosts epidemic.
     */
    void DisplayEpidemicCurve(const std::vector<seir_vector>& hostsEpidemic);
#endif

    /**
     * Displays a graph of profiled times cummulated per process in the pipeline.
     * @param [in,out] profiler The profiler.
     */
    void DisplayProfiling(Epidemica::TimeProfiler& profiler);
}

