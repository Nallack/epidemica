#include <epidemica/Globals.h>


#include <epidemica/Simulation.h>
#include <epidemica/scenario/PTreeSimulationScenario.h>
#include <epidemica/state/SimulationState.h>
#include <epidemica/DeviceManager.h>
#include <epidemica/process/IProcess.h>
#include <epidemica/process/ProcessPipeline.h>
#include <epidemica/profiling/Stopwatch.h>

#include <chrono>

#include "ProgramOptions.h"

#include "renderer/MatplotlibWrapper.h"


using namespace Epidemica;
using namespace std::chrono;

Simulation* LoadSimulation(
    const program_options& options,
    std::shared_ptr<DeviceManager> deviceManager,
    std::shared_ptr<ISimulationScenario> scenario)
{
    std::cout << "implementation : " << options["implementation"].as<std::string>() << std::endl;
    std::cout << "Creating simulation..." << std::endl;

    Simulation* simulation = new Simulation(
        options["implementation"].as<std::string>(),
        deviceManager,
        scenario);

    std::cout << "loading processes.." << std::endl;

    simulation->Initialize();
   
    //Must load the scenario into the SimulationState
    simulation->LoadStateFromScenario();

    std::cout << *simulation << std::endl;

    return simulation;
}

void RunSimulation(Simulation& simulation)
{
    //Run
    Epidemica::Stopwatch stopwatch;
    stopwatch.Start();
    while (!simulation.IsDone())
    {
        simulation.Step();
    }
    stopwatch.Stop();

    //Output
    std::cout << "Simulation run time : " << stopwatch.GetTime<double>() << " seconds" << std::endl;
}

void ProcessResults(program_options& options, Simulation& simulation)
{
    //epidemic curve
    std::vector<seir_vector>& hostsEpidemic = simulation.GetHostsEpidemic();

    if (options.count("printstats"))
    {
        for (int i = 0; i < hostsEpidemic.size(); i++)
        {
            std::cout << "["
                << hostsEpidemic[i][0] << ","
                << hostsEpidemic[i][1] << ","
                << hostsEpidemic[i][2] << ","
                << hostsEpidemic[i][3] << "]" << std::endl;
        }
    }

    if (options.count("profiling"))
    {
        DisplayProfiling(*Globals::GetTimeProfiler());
    }

    //always show epicurve if possible
    if (true || options.count("epicurve"))
    {
#ifdef MATPLOTLIBCPP_PYTHON_HEADER
        DisplayEpidemicCurve(hostsEpidemic);
#else
        //throw std::runtime_error("Curve plotting currently requires Matplotlib-cpp");
#endif
    }

}
