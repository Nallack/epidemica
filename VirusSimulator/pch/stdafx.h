#include <iostream>
#include <sstream>

#ifdef MPI_FOUND
#include <mpi.h>
#endif

#ifdef OPENMP_FOUND
#include <omp.h>
#endif